package es.sergio.bataller.salmeron.chattrip.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;
import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.fragments.AmigosFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.PostsFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.ViajesFragment;

public class TabAdapterV2 extends FragmentPagerAdapter {
    private Context context;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private final ArrayList<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mFragmentTitle = new ArrayList<>();
    private List<String> mFragmentCount = new ArrayList<>();


    public TabAdapterV2(FragmentManager manager, Context context, ViewPager viewPager, TabLayout tabLayout) {
        super(manager);
        this.context = context;
        this.viewPager = viewPager;
        this.tabLayout = tabLayout;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new PostsFragment();

        } else if (position == 1) {
            fragment = new ViajesFragment();

        } else if (position == 2) {
            fragment = new AmigosFragment();

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public View getTabView(final int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab_main, null);
        TextView textTab1 =  v.findViewById(R.id.tab_item_text1);
        TextView textTab2 =  v.findViewById(R.id.tab_item_text2);
        mFragmentCount=new ArrayList<>();
        mFragmentTitle=new ArrayList<>();
        textTab1.setText(mFragmentTitle.get(position));
        textTab2.setText(mFragmentCount.get(position));
        return v;
    }

    public void addFrag( String title, String count) {

        mFragmentTitle.add(title);
        mFragmentCount.add(count);
    }

}
