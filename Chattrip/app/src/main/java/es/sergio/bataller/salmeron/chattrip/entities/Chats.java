package es.sergio.bataller.salmeron.chattrip.entities;

import java.io.Serializable;
import java.util.List;

public class Chats implements Serializable {
    private String id;
    private Creator creator;
    private ChatsDisplay display;
    private List<Creator> active_users;
    private String[] pending_users;
    private String[] out_of_chat_users;
    private boolean not_read;
    private String last_message;
    private String last_updated;
    private String chat_name;
    private boolean is_group;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public ChatsDisplay getDisplay() {
        return display;
    }

    public void setDisplay(ChatsDisplay display) {
        this.display = display;
    }

    public List<Creator> getActive_users() {
        return active_users;
    }

    public void setActive_users(List<Creator> active_users) {
        this.active_users = active_users;
    }

    public String[] getPending_users() {
        return pending_users;
    }

    public void setPending_users(String[] pending_users) {
        this.pending_users = pending_users;
    }

    public String[] getOut_of_chat_users() {
        return out_of_chat_users;
    }

    public void setOut_of_chat_users(String[] out_of_chat_users) {
        this.out_of_chat_users = out_of_chat_users;
    }

    public boolean isNot_read() {
        return not_read;
    }

    public void setNot_read(boolean not_read) {
        this.not_read = not_read;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getChat_name() {
        return chat_name;
    }

    public void setChat_name(String chat_name) {
        this.chat_name = chat_name;
    }

    public boolean isIs_group() {
        return is_group;
    }

    public void setIs_group(boolean is_group) {
        this.is_group = is_group;
    }
}
