package es.sergio.bataller.salmeron.chattrip.fragments.bottom_nav;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.sergio.bataller.salmeron.chattrip.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LocationNavFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationNavFragment extends Fragment {



    public LocationNavFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_nav, container, false);
    }
}