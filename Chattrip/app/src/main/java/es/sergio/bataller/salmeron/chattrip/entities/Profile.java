package es.sergio.bataller.salmeron.chattrip.entities;

import java.io.Serializable;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.utils.Pager;

public class Profile implements Serializable {

    private String id;
    private String username;
    private String first_name;
    private String last_name;
    private String bio;
    private String birth_date;
    private String gender;
    private String city;
    private String country;
    private Media image_medias;
    private String email;
    private Media cover_medias;
    private Media cover_cropped_medias;
    private boolean show_city;
    private String firebase_id;
    private String language;
    private List<UserTrips> user_trips;
    private String[] blocked_users;
    private String is_friend;
    private int friends_count;
    private boolean social_register;
    private boolean is_disabled_by_admin;
    private String instagram_link;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Media getCover_medias() {
        return cover_medias;
    }

    public void setCover_medias(Media cover_medias) {
        this.cover_medias = cover_medias;
    }

    public Media getCover_cropped_medias() {
        return cover_cropped_medias;
    }

    public void setCover_cropped_medias(Media cover_cropped_medias) {
        this.cover_cropped_medias = cover_cropped_medias;
    }

    public boolean isShow_city() {
        return show_city;
    }

    public void setShow_city(boolean show_city) {
        this.show_city = show_city;
    }

    public String getFirebase_id() {
        return firebase_id;
    }

    public void setFirebase_id(String firebase_id) {
        this.firebase_id = firebase_id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String[] getBlocked_users() {
        return blocked_users;
    }

    public void setBlocked_users(String[] blocked_users) {
        this.blocked_users = blocked_users;
    }

    public String getIs_friend() {
        return is_friend;
    }

    public void setIs_friend(String is_friend) {
        this.is_friend = is_friend;
    }

    public boolean isSocial_register() {
        return social_register;
    }

    public void setSocial_register(boolean social_register) {
        this.social_register = social_register;
    }

    public boolean isIs_disabled_by_admin() {
        return is_disabled_by_admin;
    }

    public void setIs_disabled_by_admin(boolean is_disabled_by_admin) {
        this.is_disabled_by_admin = is_disabled_by_admin;
    }

    public List<UserTrips> getUser_trips() {
        return user_trips;
    }

    public void setUser_trips(List<UserTrips> user_trips) {
        this.user_trips = user_trips;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public String getInstagram_link() {
        return instagram_link;
    }

    public void setInstagram_link(String instagram_link) {
        this.instagram_link = instagram_link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Media getImage_medias() {
        return image_medias;
    }

    public void setImage_medias(Media image_medias) {
        this.image_medias = image_medias;
    }


    public Boolean getShow_city() {
        return show_city;
    }

    public void setShow_city(Boolean show_city) {
        this.show_city = show_city;
    }

    public int getFrinds_count() {
        return friends_count;
    }

    public void setFrinds_count(int frinds_count) {
        this.friends_count = frinds_count;
    }

    public Boolean getSocial_register() {
        return social_register;
    }

    public void setSocial_register(Boolean social_register) {
        this.social_register = social_register;
    }

    public Boolean getIs_disabled_by_admin() {
        return is_disabled_by_admin;
    }

    public void setIs_disabled_by_admin(Boolean is_disabled_by_admin) {
        this.is_disabled_by_admin = is_disabled_by_admin;
    }
}
