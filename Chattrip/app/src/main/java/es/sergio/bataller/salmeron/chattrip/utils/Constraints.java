package es.sergio.bataller.salmeron.chattrip.utils;

public class Constraints {

    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String SHORT_DATE_PATTERN = "dd MMM";
    public static final String MEDIUM_DATE_PATTERN = "dd MMM yyyy";
    public static final String SERVER_DATE_PATTERN = "yyyy-MM-dd";
    public static final String SERVER_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static final String FORMAT_VIDEO = ".mp4";
    public static final String FORMAT_PHOTO = ".jpg";

    // Possible server code responses
    public static final int SERVER_SUCCESS_CODE = 200;
    public static final int SERVER_CREATED_CODE = 201;
    public static final int SERVER_NOCONTENT_CODE = 204;
    public static final int SERVER_BADREQUEST_CODE = 400;
    public static final int SERVER_UNAUTHORIZED_CODE = 401;
    public static final int SERVER_FORBIDDEN_CODE = 403;
    public static final int SERVER_NOTFOUND_CODE = 404;
    public static final int SERVER_TIMEOUT_CODE = 408;
    public static final int SERVER_INTERNALSERVER_CODE = 500;

}
