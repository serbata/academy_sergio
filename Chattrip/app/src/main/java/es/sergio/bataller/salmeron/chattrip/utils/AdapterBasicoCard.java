package es.sergio.bataller.salmeron.chattrip.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Cities;
import es.sergio.bataller.salmeron.chattrip.entities.UserTrips;

public class AdapterBasicoCard extends RecyclerView.Adapter<AdapterBasicoCard.TripsViewHolder> {
    private Context context;
    private List<Cities> data;

    public AdapterBasicoCard(Context context, List<Cities> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public TripsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_viajes_item, parent, false);
        return new TripsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TripsViewHolder holder, int position) {
        holder.text_viajes_city.setText(data.get(position).getName());
        holder.text_viajes_country.setText(data.get(position).getCountry().getName());
        holder.text_primerafecha.setText(data.get(position).getCountry().getCode());

        try {
            Picasso.get()
                    .load(data.get(position).getMedias().get(0).getMidsize())
                    .into(holder.card_image_big);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class TripsViewHolder extends RecyclerView.ViewHolder {
        private ImageView card_image_big, viajes_image_calendar, image_view4;
        private TextView text_primerafecha, text_segundafecha, text_viajes_city, text_viajes_country;

        public TripsViewHolder(@NonNull View viewItem) {
            super(viewItem);
            card_image_big = viewItem.findViewById(R.id.card_image_big);
            viajes_image_calendar = viewItem.findViewById(R.id.viajes_imageCalendar);
            image_view4 = viewItem.findViewById(R.id.imageView4);
            text_primerafecha = viewItem.findViewById(R.id.text_primerafecha);
            text_segundafecha = viewItem.findViewById(R.id.text_segundafecha);
            text_viajes_city = viewItem.findViewById(R.id.viajes_textCity);
            text_viajes_country = viewItem.findViewById(R.id.viajes_textCountry);
        }
    }
}
