package es.sergio.bataller.salmeron.chattrip.entities;

import java.io.Serializable;
import java.util.List;

public class Cities implements Serializable {
    private int id;
    private String name;
    private Country country;
    private List<Media> medias;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }
}
