package es.sergio.bataller.salmeron.chattrip.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import es.sergio.bataller.salmeron.chattrip.R;

public class TestActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_coordinator);
        tabLayout = findViewById(R.id.test_tablayout);

        tabLayout.addTab(tabLayout.newTab().setText("Hello"));
        tabLayout.addTab(tabLayout.newTab().setText("Hello2"));
        tabLayout.addTab(tabLayout.newTab().setText("Hello3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


    }
}