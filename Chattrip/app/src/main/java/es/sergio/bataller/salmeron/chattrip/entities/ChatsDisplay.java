package es.sergio.bataller.salmeron.chattrip.entities;

import java.io.Serializable;
import java.util.List;

public class ChatsDisplay implements Serializable {
    private String chat_name;
    private List<Media> chat_images;

    public String getChat_name() {
        return chat_name;
    }

    public void setChat_name(String chat_name) {
        this.chat_name = chat_name;
    }

    public List<Media> getChat_images() {
        return chat_images;
    }

    public void setChat_images(List<Media> chat_images) {
        this.chat_images = chat_images;
    }
}
