package es.sergio.bataller.salmeron.chattrip.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Cities;
import es.sergio.bataller.salmeron.chattrip.entities.UserTrips;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.AdapterBasicoCard;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;


public class ViajesFragment extends Fragment {


    private RecyclerView contenedor;
    private AdapterBasicoCard adapter;
    private NestedScrollView nestedScrollView;
    private int page = 1;


    private List<Cities> listTrips = new ArrayList<>();
    private ProgressBar progressBar;


    public ViajesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = initListener(inflater, container);


        // Inflate the layout for this fragment
        return v;
    }

    @NotNull
    private View initListener(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.fragment_viajes, container, false);
        nestedScrollView = v.findViewById(R.id.viajes_nestedScrollVieW);
        contenedor = v.findViewById(R.id.viajes_recycler_fragment);
        progressBar = v.findViewById(R.id.viajes_progressbar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        adapter = new AdapterBasicoCard(getContext(), listTrips);
        contenedor.setLayoutManager(linearLayoutManager);
        contenedor.setAdapter(adapter);
        getData(page);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Check condition
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    //When reach last item position
                    //Increase page size
                    page++;
                    //Show progress bar
                    progressBar.setVisibility(View.VISIBLE);

                    //Call metod
                    getData(page);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
        return v;
    }

    public void getData(int page) {
        new DataWebService().getAllTrips(page, new DataStrategy.InteractDispatcherPager() {
            @Override
            public void response(int code, Pager pager) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {

                    listTrips.addAll(pager.getResults());
                    adapter = new AdapterBasicoCard(getContext(), listTrips);
                    adapter.notifyDataSetChanged();
                    contenedor.setAdapter(adapter);
                } else {
                    Toast.makeText(getContext(), "We have a problem in cities", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


}