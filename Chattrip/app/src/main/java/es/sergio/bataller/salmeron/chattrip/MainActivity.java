package es.sergio.bataller.salmeron.chattrip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;

public class MainActivity extends AppCompatActivity {
    private TextView txtTab1, txtTab2;
    private ViewPager2 viewPager2;
    private TabLayout tabLayout;
    private List<Profile> listaProfile;
    private LinearLayout linearLayout;
    private Context context;

    CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chattrip_main);
        viewPager2 = findViewById(R.id.chattrip_viewpager);
        collapsingToolbarLayout = findViewById(R.id.chattrip_collapsionToolbar);
        tabLayout = findViewById(R.id.chattrip_tabLayout);

        initRecogeDatos();

        //  TAbViewCustom(tabLayout,"4","AMIGOS",0);
        //  TAbViewCustom(tabLayout,"0","TRIPS",1);
        //  TAbViewCustom(tabLayout,"1","POSTS",2);


    }

    private void TabCustomView(TabLayout tab, String count, String name, int posicion) {

        linearLayout = (LinearLayout) LayoutInflater.from(getBaseContext()).inflate(R.layout.custom_tab_main, null);
        txtTab1 = linearLayout.findViewById(R.id.tab_item_text1);
        txtTab2 = linearLayout.findViewById(R.id.tab_item_text2);
        txtTab1.setText(name);
        txtTab2.setText(count);
        tab.getTabAt(posicion).setCustomView(linearLayout);

    }


//  private void TAbViewCustom(TabLayout.Tab tab, String count,String name, int posicion) {

//      View linearLayout = (View) LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_tab_main,null);


//      txtTab1 = linearLayout.findViewById(R.id.tab_item_text1);
//      txtTab2=linearLayout.findViewById(R.id.tab_item_text2);

//      txtTab1.setText(name);
//      txtTab2.setText(count);
//      if (linearLayout==null){

//

//      }
//  }

    /**
     * Antiguo Tab En el Main
     *

    private void TabViewPager() {

        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(
                tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.custom_tab_main, null);
                switch (position) {
                    case 0: {
                        tab.setCustomView(linearLayout);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setTextColor(getColor(R.color.black));
                        txtTab1.setText("POSTS");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText("COUNT");
                        txtTab2.setTextColor(getColor(R.color.black));
                        break;
                    }
                    case 1: {

                        tab.setCustomView(linearLayout);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setText("VIAJES");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText("COUNT");
                        break;
                    }
                    case 2: {

                        tab.setCustomView(linearLayout);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setText("AMIGOS");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText("COUNT");
                        BadgeDrawable badgeDrawable = tab.getOrCreateBadge();
                        badgeDrawable.setBackgroundColor(
                                ContextCompat.getColor(getApplicationContext(), R.color.black)
                        );
                        badgeDrawable.setVisible(true);
                        badgeDrawable.setNumber(100);
                        badgeDrawable.setMaxCharacterCount(3);
                        break;
                    }
                }
            }
        }
        );

        if (tabLayoutMediator.isAttached()) {
            tabLayoutMediator.attach();
        }


    }
     */

    private void initRecogeDatos() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {
                    int i = 0;
                    Profile profile = (Profile) object;
                    if (profile.getFirst_name().isEmpty()) {
                        listaProfile.add((Profile) object);
                        if (listaProfile.isEmpty()) {
                            Toast.makeText(MainActivity.this, "Mama la que hemos liado", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        collapsingToolbarLayout.setTitle(profile.getFirst_name());
                        TabCustomView(tabLayout, "2", "NEJ", 0);
                        TabCustomView(tabLayout, "2", "NEJ", 1);
                        TabCustomView(tabLayout, "2", "NEJ", 2);

                    }

                }
            }
        });


    }
}