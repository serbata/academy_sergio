package es.sergio.bataller.salmeron.chattrip.interfaces;

import android.util.Log;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import es.sergio.bataller.salmeron.chattrip.App;
import es.sergio.bataller.salmeron.chattrip.BuildConfig;
import es.sergio.bataller.salmeron.chattrip.entities.Chats;
import es.sergio.bataller.salmeron.chattrip.entities.Cities;
import es.sergio.bataller.salmeron.chattrip.entities.Friends;
import es.sergio.bataller.salmeron.chattrip.entities.Login;
import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.entities.User;
import es.sergio.bataller.salmeron.chattrip.entities.UserTrips;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DataWebService extends DataStrategy {


    private Retrofit retrofit;
    private Retrofit retrofitLogin;
    private ApiService apiService;
    private ApiService apiserviceLogin;
    private String TAG_FAILURE = "FAILURE_CALL";


    public DataWebService() {

        OkHttpClient clientWithoutAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .build();

        OkHttpClient clientWithAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .addInterceptor(new AccessTokenInterceptor())
                .authenticator(new AccessTokenAuthenticator())
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Configuracion.API_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientWithAuth)
                .build();

        apiService = retrofit.create(ApiService.class);

        retrofitLogin = new Retrofit.Builder()
                .baseUrl(Configuracion.API_URL)
                .client(clientWithoutAuth)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiserviceLogin = retrofitLogin.create(ApiService.class);

    }

    @Override
    public Call<Token> refreshToken() {
        ApiService apiServiceRefresh = retrofitLogin.create(ApiService.class);

        Login login = new Login();
        login.setRefresh_token(App.preferences.getRefreshToken());
        login.setGrant_type(Configuracion.GRANT_TYPE);

        return apiServiceRefresh.refreshToken(login);
    }

    @Override
    public void getFireBaseToken(InteractDispatcherGeneric interactDispatcherGeneric) {
        apiService.getFirebaseToken().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                interactDispatcherGeneric.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                interactDispatcherGeneric.response(Constraints.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get firebase token - " + t.getMessage());
            }
        });
    }

    @Override
    public void postRegister(User profile, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postRegister(profile).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                interactDispatcherObject.response(Constraints.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post register - " + t.getMessage());
            }
        });
    }

    @Override
    public void postLogin(Login login, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postLogin(login).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                interactDispatcherObject.response(Constraints.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post login - " + t.getMessage());
            }
        });
    }

    @Override
    public void getMe(InteractDispatcherObject interactDispatcherObject) {
        apiService.getMe().enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());

            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get me - " + t.getMessage());
            }
        });
    }



    @Override
    public void getUserTrips(InteractDispatcherPager interactDispatcherPager) {
        apiService.getUserTrips().enqueue(new Callback<Pager<UserTrips>>() {
            @Override
            public void onResponse(Call<Pager<UserTrips>> call, Response<Pager<UserTrips>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<UserTrips>> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get usertrips - " + t.getMessage());
            }
        });
    }

    @Override
    public void getAllTrips(int page, InteractDispatcherPager interactDispatcherPager) {
        apiService.getAllTrips(page).enqueue(new Callback<Pager<Cities>>() {
            @Override
            public void onResponse(Call<Pager<Cities>> call, Response<Pager<Cities>> response) {
                interactDispatcherPager.response(response.code(),response.body());
            }

            @Override
            public void onFailure(Call<Pager<Cities>> call, Throwable t) {
                Log.e(TAG_FAILURE,"onFailure : get Cities - "+ t.getMessage());

            }
        });
    }



    @Override
    public void getAllFriends(String user, int id, InteractDispatcherPager interactDispatcherPager) {
        apiService.getAllFriends(user, id).enqueue(new Callback<Pager<Friends>>() {
            @Override
            public void onResponse(Call<Pager<Friends>> call, Response<Pager<Friends>> response) {
                interactDispatcherPager.response(response.code(),response.body());
            }

            @Override
            public void onFailure(Call<Pager<Friends>> call, Throwable t) {
            Log.e(TAG_FAILURE,"onF>ailure : getAllFriends - "+t.getMessage());
            }
        });
    }

    @Override
    public void getFriends(String user, InteractDispatcherPager interactDispatcherPager) {
        apiService.getFriends(user).enqueue(new Callback<Pager<Friends>>() {
            @Override
            public void onResponse(Call<Pager<Friends>> call, Response<Pager<Friends>> response) {
                interactDispatcherPager.response(response.code(),response.body());
            }

            @Override
            public void onFailure(Call<Pager<Friends>> call, Throwable t) {
            Log.e(TAG_FAILURE, "onFailure : getFriends - "+t.getMessage());
            }
        });
    }

    @Override
    public void getChats(InteractDispatcherPager interactDispatcherPager) {
        apiService.getChats().enqueue(new Callback<Pager<Chats>>() {
            @Override
            public void onResponse(Call<Pager<Chats>> call, Response<Pager<Chats>> response) {
                interactDispatcherPager.response(response.code(),response.body());
            }

            @Override
            public void onFailure(Call<Pager<Chats>> call, Throwable t) {
            Log.e(TAG_FAILURE,"onFailure : getChats - "+t.getMessage());
            }
        });
    }
}