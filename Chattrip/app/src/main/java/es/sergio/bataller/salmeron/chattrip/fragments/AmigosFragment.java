package es.sergio.bataller.salmeron.chattrip.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Friends;
import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.entities.User;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.FragmentAdapter;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;


public class AmigosFragment extends Fragment {
    private List<Friends> listaUsers = new ArrayList<>();
    private RecyclerView contenedor;
    private String userID;
    private int page = 1;
    private Profile profile;


    private FragmentAdapter adapter;

    public AmigosFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getUserData();


        // listaUsers.add(new User(1,"serbata","rake@hotmail.com","1234","Pepe","65462737"));
        // listaUsers.add(new User(2,"serbata2","rake1@hotmail.com","1233","Juan","65462737"));
        // listaUsers.add(new User(3,"serbata3","rake2@hotmail.com","1232","Silvia","65462737"));
        // listaUsers.add(new User(4,"serbata4","rake3@hotmail.com","1231","Maria","65462737"));
        // listaUsers.add(new User(5,"serbata5","rake4@hotmail.com","1235","TIA","65462737"));
        // listaUsers.add(new User(6,"serbata6","rake5@hotmail.com","1236","PEP","65462737"));
        // listaUsers.add(new User(7,"serbata7","rake6@hotmail.com","1237","Manolito","65462737"));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_amigos, container, false);
        contenedor = v.findViewById(R.id.recycler_amigos);
        adapter = new FragmentAdapter(getContext(), listaUsers);
        contenedor.setLayoutManager(new LinearLayoutManager(getActivity()));
        contenedor.setAdapter(adapter);
        // Inflate the layout for this fragment
        return v;
    }

    public void getData(String id, int page) {
        new DataWebService().getAllFriends(id, page, new DataStrategy.InteractDispatcherPager() {
            @Override
            public void response(int code, Pager pager) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {
                    listaUsers.addAll(pager.getResults());
                    adapter = new FragmentAdapter(getContext(), listaUsers);
                    adapter.notifyDataSetChanged();
                    contenedor.setAdapter(adapter);
                } else {
                    Toast.makeText(getContext(), "We have a problem with los friends", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getUserData() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {
                    Profile profile = (Profile) object;
                    userID = profile.getId();
                    getData(userID, page);
                }
            }
        });
    }
}