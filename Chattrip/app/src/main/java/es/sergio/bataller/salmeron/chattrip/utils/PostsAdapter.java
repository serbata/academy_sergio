package es.sergio.bataller.salmeron.chattrip.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Chats;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ChatsVH> {
    private Context context;
    private List<Chats> objects;

    public PostsAdapter(Context context, List<Chats> objects) {
        this.context = context;
        this.objects = objects;
    }

    @NonNull
    @Override
    public ChatsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_posts,parent,false);
        return new ChatsVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsVH holder, int position) {
        Chats chats=objects.get(position);

        try{
            Picasso.get()
                    .load(chats.getDisplay().getChat_images().get(position).getThumbnail())
                    .resize(68,68)
                    .into(holder.image_chats);
        }catch (Exception e){
            e.printStackTrace();
        }
        holder.txtFitstNameCard.setText(chats.getChat_name());
        holder.txtUserNameCard.setText(chats.getCreator().getUsername());
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class ChatsVH extends RecyclerView.ViewHolder {
        private ImageView image_chats;
        private TextView txtUserNameCard, txtFitstNameCard;
        public ChatsVH(@NonNull View itemView) {
            super(itemView);
            image_chats=itemView.findViewById(R.id.posts_image);
            txtFitstNameCard=itemView.findViewById(R.id.posts_text1);
            txtUserNameCard=itemView.findViewById(R.id.posts_text2);
        }
    }
}
