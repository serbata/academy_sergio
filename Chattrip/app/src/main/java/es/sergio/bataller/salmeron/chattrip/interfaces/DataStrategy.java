package es.sergio.bataller.salmeron.chattrip.interfaces;

import java.util.List;

import es.sergio.bataller.salmeron.chattrip.entities.Chats;
import es.sergio.bataller.salmeron.chattrip.entities.Cities;
import es.sergio.bataller.salmeron.chattrip.entities.Friends;
import es.sergio.bataller.salmeron.chattrip.entities.Login;
import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.entities.User;
import es.sergio.bataller.salmeron.chattrip.entities.UserTrips;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public abstract class DataStrategy {
    public abstract Call<Token> refreshToken();


    public abstract void getFireBaseToken(InteractDispatcherGeneric interactDispatcherGeneric);

    public abstract void postLogin(Login login, InteractDispatcherObject interactDispatcherObject);

    public abstract void postRegister(User user, InteractDispatcherObject interactDispatcherObject);

    public abstract void getMe(InteractDispatcherObject interactDispatcherObject);
    public abstract void getUserTrips(InteractDispatcherPager interactDispatcherPager);
    public abstract void getAllTrips(int page,InteractDispatcherPager interactDispatcherPager);
    public abstract void getAllFriends(String user,int id,InteractDispatcherPager interactDispatcherPager);
    public abstract void getFriends(String user,InteractDispatcherPager interactDispatcherPager);
    public abstract void getChats(InteractDispatcherPager interactDispatcherPager);

    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface InteractDispatcherListObject<T> {
        void response(int code, List<T> object);
    }

    public interface InteractDispatcherGeneric {
        void response(int code, String message);
    }
    public interface InteractDispatcherPager<T> {
        void response(int code, Pager<T> pager);
    }


    public interface ApiService {
        //REFRESH TOKEN
        @POST("auth/token/")
        Call<Token> refreshToken(@Body Login login);

        //GET FIREBASE TOKEN
        @GET("users/firebase_token/")
        Call<String> getFirebaseToken();

        //GET ME
        @GET("users/me/")
        Call<Profile> getMe();

        //LOGIN
        @POST("auth/token")
        Call<Token> postLogin(@Body Login login);

        //REGISTER
        @POST("auth/register/")
        Call<User> postRegister(@Body User register);

        //GET USER TRIPS
        @GET("trips/")
        Call<Pager<UserTrips>> getUserTrips();

        //GET ALL TRIPS
        @GET("cities/")
        Call<Pager<Cities>> getAllTrips(@Query("page") int page);

        //GET FRIENDS
        @GET("users/friends/")
        Call<Pager<Friends>> getAllFriends(@Query("user")String user,
                                           @Query("page")int id);

        //Get Friends
        @GET("users/friends/")
        Call<Pager<Friends>> getFriends(@Query("user") String user);

        //GET CHATS
        @GET("chats/")
        Call<Pager<Chats>> getChats();
    }
}
