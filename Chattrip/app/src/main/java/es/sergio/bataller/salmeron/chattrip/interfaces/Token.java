package es.sergio.bataller.salmeron.chattrip.interfaces;

public class Token {
    private String access_token, token_type,
            scope,refresh_token;
    private long expires_in;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public long getExpire_in() {
        return expires_in;
    }

    public void setExpire_in(long expire_in) {
        this.expires_in = expire_in;
    }
}
