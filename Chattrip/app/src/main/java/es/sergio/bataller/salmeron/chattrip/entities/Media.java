package es.sergio.bataller.salmeron.chattrip.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Media implements Serializable {

    private String file;
    private String thumbnail;
    private String midsize;
    private String fullsize;


    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMidsize() {
        return midsize;
    }

    public void setMidsize(String midsize) {
        this.midsize = midsize;
    }

    public String getFullsize() {
        return fullsize;
    }

    public void setFullsize(String fullsize) {
        this.fullsize = fullsize;
    }

}
