package es.sergio.bataller.salmeron.chattrip.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Chats;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;
import es.sergio.bataller.salmeron.chattrip.utils.PostsAdapter;


public class PostsFragment extends Fragment {
    private RecyclerView contenedor;
    private PostsAdapter adapter;
    private List<Chats> listChats=new ArrayList<>();

    public PostsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_posts,container,false);
        contenedor=v.findViewById(R.id.posts_recycler);
        adapter=new PostsAdapter(getContext(),listChats);
        contenedor.setLayoutManager(new LinearLayoutManager(getActivity()));
        contenedor.setAdapter(adapter);
        getChats();
        return v;
    }
    
    public void getChats(){
        new DataWebService().getChats(new DataStrategy.InteractDispatcherPager() {
            @Override
            public void response(int code, Pager pager) {
                if (code == Constraints.SERVER_SUCCESS_CODE){
                    listChats.addAll(pager.getResults());
                    adapter=new PostsAdapter(getContext(),listChats);
                    adapter.notifyDataSetChanged();
                    contenedor.setAdapter(adapter);
                }
                else{
                    Toast.makeText(getContext(), "Algo no funciono bien", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}