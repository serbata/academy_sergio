package es.sergio.bataller.salmeron.chattrip.entities;

import java.io.Serializable;
import java.util.List;

public class Friends implements Serializable {
    private String id;
    private String username;
    private String first_name;
    private String last_name;
    private String bio;
    private String city;
    private String country;
    private Media image_medias;
    private Media cover_medias;
    private Media cover_cropped_medias;
    private boolean show_city;
    private String firebase_id;
    private String language;
    private String is_friend;
    private int friends_count;
    private boolean is_blocked;
    private boolean social_register;
    private boolean is_disabled_by_admin;
    private boolean instagram_link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Media getImage_medias() {
        return image_medias;
    }

    public void setImage_medias(Media image_medias) {
        this.image_medias = image_medias;
    }

    public Media getCover_medias() {
        return cover_medias;
    }

    public void setCover_medias(Media cover_medias) {
        this.cover_medias = cover_medias;
    }

    public Media getCover_cropped_medias() {
        return cover_cropped_medias;
    }

    public void setCover_cropped_medias(Media cover_cropped_medias) {
        this.cover_cropped_medias = cover_cropped_medias;
    }

    public boolean isShow_city() {
        return show_city;
    }

    public void setShow_city(boolean show_city) {
        this.show_city = show_city;
    }

    public String getFirebase_id() {
        return firebase_id;
    }

    public void setFirebase_id(String firebase_id) {
        this.firebase_id = firebase_id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIs_friend() {
        return is_friend;
    }

    public void setIs_friend(String is_friend) {
        this.is_friend = is_friend;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public boolean isIs_blocked() {
        return is_blocked;
    }

    public void setIs_blocked(boolean is_blocked) {
        this.is_blocked = is_blocked;
    }

    public boolean isSocial_register() {
        return social_register;
    }

    public void setSocial_register(boolean social_register) {
        this.social_register = social_register;
    }

    public boolean isIs_disabled_by_admin() {
        return is_disabled_by_admin;
    }

    public void setIs_disabled_by_admin(boolean is_disabled_by_admin) {
        this.is_disabled_by_admin = is_disabled_by_admin;
    }

    public boolean isInstagram_link() {
        return instagram_link;
    }

    public void setInstagram_link(boolean instagram_link) {
        this.instagram_link = instagram_link;
    }
}
