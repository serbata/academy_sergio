package es.sergio.bataller.salmeron.chattrip.fragments.bottom_nav;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Chats;
import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.fragments.AmigosFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.PostsFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.ViajesFragment;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.Pager;
import es.sergio.bataller.salmeron.chattrip.utils.TabAdapter;
import es.sergio.bataller.salmeron.chattrip.utils.TabAdapterV2;


public class ProfileNavFragment extends Fragment {
    private LinearLayout linearLayout;
    private TextView txtTab1, txtTab2;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Toolbar profileToolbar;
    private FloatingActionButton fabSettingBig, fabSettingLittle;
    private AppBarLayout appbar;


    public ProfileNavFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile_nav, container, false);

        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = view.findViewById(R.id.chattrip_viewpager);
        tabLayout = view.findViewById(R.id.chattrip_tabLayout);
        profileToolbar = view.findViewById(R.id.chattrip_toolbar01);
        fabSettingBig = view.findViewById(R.id.chattrip_profile_settings_fab);
        fabSettingLittle=view.findViewById(R.id.chattrip_collapse_settings);
        appbar=view.findViewById(R.id.chattrip_appBarLayout);
        fabSettingBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Esto son ajustes",Snackbar.LENGTH_LONG).show();
            }
        });

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    //Collapsed
                    fabSettingBig.setEnabled(false);
                    fabSettingLittle.setEnabled(true);

                } else if (verticalOffset == 0) {
                    //Expanded
                    fabSettingBig.setEnabled(true);
                    fabSettingLittle.setEnabled(false);
                } else {
                    fabSettingBig.setEnabled(true);
                    fabSettingLittle.setEnabled(false);
                }
            }
        });

        ((AppCompatActivity)getActivity()).setSupportActionBar(profileToolbar);

        collapsingToolbarLayout = view.findViewById(R.id.chattrip_collapsionToolbar);
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getContext(), android.R.color.black));


        ListenerViewPager(viewPager);
        initRecogeDatos();


    }


    private void initRecogeDatos() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {
                    Profile profile = (Profile) object;
                    if (!profile.getFirst_name().isEmpty()) {
                        collapsingToolbarLayout.setTitle(profile.getFirst_name());
                        collapsingToolbarLayout.setCollapsedTitleGravity(View.TEXT_ALIGNMENT_CENTER);
                        profileToolbar.setTitle(profile.getFirst_name());
                        profile.setId(((Profile) object).getId());

                        new DataWebService().getChats(new DataStrategy.InteractDispatcherPager() {
                            @Override
                            public void response(int code, Pager pager) {
                                if (code==Constraints.SERVER_SUCCESS_CODE){
                                    List<Chats> chats=new ArrayList<>();
                                    chats.addAll(pager.getResults());

                                    TabView(tabLayout, String.valueOf(chats.size()), "POSTS", 0);
                                    TabView(tabLayout, String.valueOf(profile.getUser_trips().size()), "VIAJES", 1);
                                    TabView(tabLayout, String.valueOf(profile.getFrinds_count()), "AMIGOS", 2);

                                }
                            }
                        });
                        // tabAdapterV2 = new TabAdapterV2(getChildFragmentManager(), getContext(), viewPager, tabLayout);
                        // TabView(tabLayout,"02","POSTS",0);
                        // TabView(tabLayout,String.valueOf(profile.getUser_trips().size()),"VIAJES",1);
                        // TabView(tabLayout,String.valueOf(profile.getFrinds_count()),"AMIGOS",2);

                    }

                }
            }
        });
    }

    private void ListenerViewPager(ViewPager viewPager) {
        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragments(new PostsFragment(), "POSTS");
        adapter.addFragments(new ViajesFragment(), "VIAJES");
        adapter.addFragments(new AmigosFragment(), "AMIGOS");
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }

    private void TabView(TabLayout tab, String count, String name, int posicion) {
        try {
            linearLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab_main, null);

            txtTab1 = linearLayout.findViewById(R.id.tab_item_text1);
            txtTab2 = linearLayout.findViewById(R.id.tab_item_text2);
            txtTab1.setText(name);
            txtTab2.setText(count);
            tab.getTabAt(posicion).setCustomView(linearLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}