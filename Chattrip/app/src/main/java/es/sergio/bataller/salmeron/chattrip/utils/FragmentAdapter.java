package es.sergio.bataller.salmeron.chattrip.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.databinding.UserCardBinding;
import es.sergio.bataller.salmeron.chattrip.entities.Friends;
import es.sergio.bataller.salmeron.chattrip.entities.User;

public class FragmentAdapter extends RecyclerView.Adapter<FragmentAdapter.UserVH> {


    private Context context;
    private List<Friends> objects;


    public FragmentAdapter(Context context, List<Friends> objects) {
        this.context = context;
        this.objects = objects;

    }

    @NonNull
    @Override
    public FragmentAdapter.UserVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_card, parent, false);
        return new UserVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FragmentAdapter.UserVH holder, int position) {
        Friends user = objects.get(position);
        holder.txtUserNameCard.setText(user.getFirst_name());
        holder.txtUserEmailCard.setText(user.getUsername());
        try{
            Picasso.get()
                    .load(user.getImage_medias().getThumbnail())
                    .into(holder.imageFriends);
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class UserVH extends RecyclerView.ViewHolder {
        private TextView txtUserNameCard, txtUserEmailCard;
        private ImageView imageFriends;

        public UserVH(View itemView) {
            super(itemView);
            txtUserNameCard = itemView.findViewById(R.id.user_name_card);
            txtUserEmailCard = itemView.findViewById(R.id.user_email_card);
            imageFriends = itemView.findViewById(R.id.user_call_card);


        }
    }
}
