package es.sergio.bataller.salmeron.loginactivity.utils;

import android.widget.Scroller;

import androidx.recyclerview.widget.RecyclerView;

public class EndlessScrollListener extends RecyclerView.OnScrollListener {
    private static final int MIN_POSITION_TO_END = 2;
    private final Scroller scrollerClient;

    public EndlessScrollListener(Scroller scrollerClient) {
        this.scrollerClient = scrollerClient;
    }
}
