package es.sergio.bataller.salmeron.loginactivity.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private int id_user;
    private String usuario_user;
    private String user_email;
    private String user_password;
    private String user_name;
    private String user_phone;

    public User() {
    }

    public User(int id_user, String usuario_user, String user_email, String user_password, String user_name, String user_phone) {
        this.id_user = id_user;
        this.usuario_user = usuario_user;
        this.user_email = user_email;
        this.user_password = user_password;
        this.user_name = user_name;
        this.user_phone = user_phone;
    }

    protected User(Parcel in) {
        id_user = in.readInt();
        usuario_user = in.readString();
        user_email = in.readString();
        user_password = in.readString();
        user_name = in.readString();
        user_phone=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_user);
        dest.writeString(usuario_user);
        dest.writeString(user_email);
        dest.writeString(user_password);
        dest.writeString(user_name);
        dest.writeString(user_phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public es.sergio.bataller.salmeron.chattrip.entities.User createFromParcel(Parcel in) {
            return new es.sergio.bataller.salmeron.chattrip.entities.User(in);
        }

        @Override
        public es.sergio.bataller.salmeron.chattrip.entities.User[] newArray(int size) {
            return new es.sergio.bataller.salmeron.chattrip.entities.User[size];
        }
    };

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUsuario_user() {
        return usuario_user;
    }

    public void setUsuario_user(String usuario_user) {
        this.usuario_user = usuario_user;
    }


    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }
}
