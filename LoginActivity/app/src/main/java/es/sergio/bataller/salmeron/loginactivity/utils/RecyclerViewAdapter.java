package es.sergio.bataller.salmeron.loginactivity.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.sergio.bataller.salmeron.loginactivity.R;
import es.sergio.bataller.salmeron.loginactivity.entities.UserTrips;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    private List<UserTrips> listaUserTrips;
    private static final int LOADING = 0;
    private static final int ITEM = 1;
    private boolean isLoadingAdded = false;


    public RecyclerViewAdapter(Context context, List<UserTrips> listaUserTrips) {
        this.context = context;
        this.listaUserTrips = listaUserTrips;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.card_viajes_item, parent, false);
                viewHolder = new TripsViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_loading, parent, false);
                viewHolder = new LoadingViewHolder(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        List<UserTrips> tripsUsuario = (List<UserTrips>) listaUserTrips.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                TripsViewHolder tripsViewHolder = (TripsViewHolder) holder;
                tripsViewHolder.text_viajes_city.setText(tripsUsuario.get(position).getDestination().getName());
                tripsViewHolder.text_viajes_country.setText(tripsUsuario.get(position).getDestination().getCountry().getName());
                tripsViewHolder.text_primerafecha.setText(tripsUsuario.get(position).getStart_date());
                tripsViewHolder.text_segundafecha.setText(tripsUsuario.get(position).getEnd_date());
                Picasso.get().load(tripsUsuario.get(position).getDestination().getMedias().get(position).getFile()).into(tripsViewHolder.card_image_big);
                break;
            case LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return listaUserTrips == null ? 0 : listaUserTrips.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listaUserTrips.size() - 1 && isLoadingAdded) ?
                LOADING : ITEM;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;

    }


    private class TripsViewHolder extends RecyclerView.ViewHolder {
        private ImageView card_image_big, viajes_image_calendar, image_view4;
        private TextView text_primerafecha, text_segundafecha, text_viajes_city, text_viajes_country;

        public TripsViewHolder(View viewItem) {
            super(viewItem);
            card_image_big = viewItem.findViewById(R.id.card_image_big);
            viajes_image_calendar = viewItem.findViewById(R.id.viajes_imageCalendar);
            image_view4 = viewItem.findViewById(R.id.imageView4);
            text_primerafecha = viewItem.findViewById(R.id.text_primerafecha);
            text_segundafecha = viewItem.findViewById(R.id.text_segundafecha);
            text_viajes_city = viewItem.findViewById(R.id.viajes_textCity);
            text_viajes_country = viewItem.findViewById(R.id.viajes_textCountry);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public LoadingViewHolder(View viewLoading) {
            super(viewLoading);
            progressBar = viewLoading.findViewById(R.id.item_loading_ProgressBar);
        }
    }
}
