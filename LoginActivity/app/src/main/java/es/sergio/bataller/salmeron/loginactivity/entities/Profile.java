package es.sergio.bataller.salmeron.loginactivity.entities;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

    private String id;
    private String username;
    private String password;
    private String password_confirm;
    private String first_name;
    private String last_name;
    private String country;
    private String email;
    private String device_id;
    private String platform;
    private List<Media> image_medias;
    private List<Media> cover_medias;
    private List<Media> cover_cropped_media;
    private String birth_date;
    private String gender;
    private String bio;
    private UserTrips user_trips;
    private Boolean show_city;
    private BlockedUsers blocked_suers;
    private Boolean is_friend;
    private int friends_count;
    private Boolean social_register;
    private Boolean is_disabled_by_admin;
    private String instagram_link=null;

    public UserTrips getUser_trips() {
        return user_trips;
    }

    public void setUser_trips(UserTrips user_trips) {
        this.user_trips = user_trips;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public String getInstagram_link() {
        return instagram_link;
    }

    public void setInstagram_link(String instagram_link) {
        this.instagram_link = instagram_link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirm() {
        return password_confirm;
    }

    public void setPassword_confirm(String password_confirm) {
        this.password_confirm = password_confirm;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }


    public Boolean getShow_city() {
        return show_city;
    }

    public void setShow_city(Boolean show_city) {
        this.show_city = show_city;
    }

    public BlockedUsers getBlocked_suers() {
        return blocked_suers;
    }

    public void setBlocked_suers(BlockedUsers blocked_suers) {
        this.blocked_suers = blocked_suers;
    }

    public Boolean getIs_friend() {
        return is_friend;
    }

    public void setIs_friend(Boolean is_friend) {
        this.is_friend = is_friend;
    }

    public int getFrinds_count() {
        return friends_count;
    }

    public void setFrinds_count(int frinds_count) {
        this.friends_count = frinds_count;
    }

    public Boolean getSocial_register() {
        return social_register;
    }

    public void setSocial_register(Boolean social_register) {
        this.social_register = social_register;
    }

    public Boolean getIs_disabled_by_admin() {
        return is_disabled_by_admin;
    }

    public void setIs_disabled_by_admin(Boolean is_disabled_by_admin) {
        this.is_disabled_by_admin = is_disabled_by_admin;
    }

    public List<Media> getImage_medias() {
        return image_medias;
    }

    public void setImage_medias(List<Media> image_medias) {
        this.image_medias = image_medias;
    }

    public List<Media> getCover_medias() {
        return cover_medias;
    }

    public void setCover_medias(List<Media> cover_medias) {
        this.cover_medias = cover_medias;
    }

    public List<Media> getCover_cropped_media() {
        return cover_cropped_media;
    }

    public void setCover_cropped_media(List<Media> cover_cropped_media) {
        this.cover_cropped_media = cover_cropped_media;
    }
}
