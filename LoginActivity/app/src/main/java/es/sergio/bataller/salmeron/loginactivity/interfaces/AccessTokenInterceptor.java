package es.sergio.bataller.salmeron.loginactivity.interfaces;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import es.sergio.bataller.salmeron.chattrip.App;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AccessTokenInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        String accessToken = App.preferences.getAccessToken();
        Request request = newRequestWithAccessToken(chain.request(), accessToken);
        return chain.proceed(request);
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header(es.sergio.bataller.salmeron.chattrip.interfaces.Configuracion.TYPE_ITEM_AUTHORIZATION, es.sergio.bataller.salmeron.chattrip.interfaces.Configuracion.HTTP_CLIENT_AUTHORIZATION + accessToken)
                .build();
    }
}
