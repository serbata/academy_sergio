package es.sergio.bataller.salmeron.loginactivity.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.sergio.bataller.salmeron.loginactivity.R;
import es.sergio.bataller.salmeron.loginactivity.entities.UserTrips;
import es.sergio.bataller.salmeron.loginactivity.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.loginactivity.interfaces.DataWebService;
import es.sergio.bataller.salmeron.loginactivity.utils.Constraints;
import es.sergio.bataller.salmeron.loginactivity.utils.Pager;
import es.sergio.bataller.salmeron.loginactivity.utils.PaginatorScrollListener;
import es.sergio.bataller.salmeron.loginactivity.utils.RecyclerViewAdapter;


public class ViajesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView contenedor;
    private RecyclerViewAdapter adapter;

    private List<UserTrips> listTrips = (List<UserTrips>) new UserTrips();
    private ProgressBar progressBar;
    private Pager<UserTrips> pagination= new Pager<>();


    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ViajesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViajesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViajesFragment newInstance(String param1, String param2) {
        ViajesFragment fragment = new ViajesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_viajes, container,false);
        contenedor =v.findViewById(R.id.viajes_recycler_fragment);
         progressBar=v.findViewById(R.id.viajes_progressBar);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        adapter = new RecyclerViewAdapter(getContext(),listTrips);
        contenedor.setLayoutManager(linearLayoutManager);
        contenedor.setAdapter(adapter);
        contenedor.addOnScrollListener(new PaginatorScrollListener(linearLayoutManager){
            @Override
            protected void loadMoreItems(){
                isLoading=true;
                currentPage +=1;
                loadNextPage();
            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public  boolean isLoading(){
                return isLoading;
            }
        });
        loadFirstPage();

        // Inflate the layout for this fragment
        return v;
    }

    private void loadFirstPage() {
        new DataWebService().getUserTrips(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE){
                    pagination = (Pager<UserTrips>) object;
                    listTrips.add((UserTrips) pagination.getResults());
                      progressBar.setVisibility(View.GONE);


                    if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage= true;
                }
            }
        });
    }

    private void loadNextPage() {
        new DataWebService().getUserTrips(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE){
                //   ArrayList<Pager<UserTrips>> results = (ArrayList<Pager<UserTrips>>) object;
                //   adapter.addAll(results);

                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage= true;
                }
            }
        });
    }




}