package es.sergio.bataller.salmeron.loginactivity.entities;

public class BlockedUsers {
    private String[] blocked_users;

    public String[] getBlocked_users() {
        return blocked_users;
    }

    public void setBlocked_users(String[] blocked_users) {
        this.blocked_users = blocked_users;
    }
}
