package es.sergio.bataller.salmeron.loginactivity.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.loginactivity.databinding.UserCardBinding;
import es.sergio.bataller.salmeron.loginactivity.entities.User;

public class FragmentAdapter extends RecyclerView.Adapter<FragmentAdapter.UserVH> {


    private Context context;
    private List<User> objects;


    public FragmentAdapter(Context context, List<User> objects) {
        this.context = context;
        this.objects = objects;

    }

    @NonNull
    @Override
    public UserVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
     UserCardBinding ViewUserBinding =UserCardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
      return  new UserVH(ViewUserBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull FragmentAdapter.UserVH holder, int position) {
    final User user = objects.get(position);
    holder.binding.userNameCard.setText(user.getUser_name());
    holder.binding.userEmailCard.setText(user.getUser_email());


    }
    public void filterList(ArrayList<User> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        objects = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class UserVH extends RecyclerView.ViewHolder {
        private final UserCardBinding binding;
        public UserVH( UserCardBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;

        }
    }
}
