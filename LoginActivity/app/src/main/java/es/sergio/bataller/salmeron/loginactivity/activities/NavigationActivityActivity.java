package es.sergio.bataller.salmeron.loginactivity.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import es.sergio.bataller.salmeron.loginactivity.R;
import es.sergio.bataller.salmeron.loginactivity.entities.UserTrips;
import es.sergio.bataller.salmeron.loginactivity.utils.Pager;

public class NavigationActivityActivity extends AppCompatActivity {

    final Fragment mCommentsNavFragment = new CommentsNavFragment();
    final Fragment mEarthNavFragment = new EarthNavFragment();
    final Fragment mLikesNavFragment = new LikesNavFragment();
    final Fragment mLocationFragment = new LocationNavFragment();
    final Fragment mProfileFragment = new ProfileNavFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = mProfileFragment;
    private BottomNavigationView bottomNavigationView;
    private Fragment firstFragment;
    private int TabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_activity);
        BottomNavigationView navView = findViewById(R.id.bottom_navigation_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadData();
        setOnCreateFragment(navView, TabId);

    }




    private void setOnCreateFragment(BottomNavigationView navigation, int tabId) {

        fm.beginTransaction().add(R.id.nav_host_fragment, mLocationFragment, "1").hide(mLocationFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, mCommentsNavFragment, "2").hide(mCommentsNavFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, mEarthNavFragment, "3").hide(mEarthNavFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, mProfileFragment, "4").hide(mProfileFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, mLikesNavFragment, "5").hide(mLikesNavFragment).commit();

        if (tabId != -1) {
            switch (tabId) {
                case R.id.menu_location:
                    fm.beginTransaction().show(mLocationFragment).commit();
                    firstFragment = mLocationFragment;
                    break;
                case R.id.menu_comment:
                    fm.beginTransaction().show(mCommentsNavFragment).commit();
                    firstFragment = mCommentsNavFragment;
                    break;
                case R.id.menu_world:
                    fm.beginTransaction().show(mEarthNavFragment).commit();
                    firstFragment = mEarthNavFragment;
                    break;
                case R.id.menu_profile:
                    fm.beginTransaction().show(mProfileFragment).commit();
                    firstFragment = mProfileFragment;
                    break;
                case R.id.menu_like:
                    fm.beginTransaction().show(mLikesNavFragment).commit();
                    firstFragment = mLikesNavFragment;
                    break;
            }
            navigation.setSelectedItemId(tabId);
        } else {
            fm.beginTransaction().show(mProfileFragment).commit();
            firstFragment = mProfileFragment;
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            TabId = item.getItemId();
            switch (TabId) {
                case R.id.menu_location:
                    fm.beginTransaction().hide(firstFragment).show(mLocationFragment).commit();
                    firstFragment = mLocationFragment;
                    break;
                case R.id.menu_comment:
                    fm.beginTransaction().hide(firstFragment).show(mCommentsNavFragment).commit();
                    firstFragment = mCommentsNavFragment;
                    break;
                case R.id.menu_world:
                    fm.beginTransaction().hide(firstFragment).show(mEarthNavFragment).commit();
                    firstFragment = mEarthNavFragment;
                    break;
                case R.id.menu_profile:
                    fm.beginTransaction().hide(firstFragment).show(mProfileFragment).commit();
                    firstFragment = mProfileFragment;
                    break;
                case R.id.menu_like:
                    fm.beginTransaction().hide(firstFragment).show(mLikesNavFragment).commit();
                    firstFragment = mLikesNavFragment;
                    break;
            }
            return true;
        }
    };


    private void loadData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        TabId = preferences.getInt("tabId", -1);
    }

    private void saveData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tabId", TabId).commit();
    }

    @Override
    protected void onStop() {
        saveData();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        fm.beginTransaction().hide(firstFragment).commitAllowingStateLoss();
        super.onDestroy();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}