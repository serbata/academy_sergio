package es.sergio.bataller.salmeron.loginactivity.fragments.bottom_nav;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import es.sergio.bataller.salmeron.chattrip.R;
import es.sergio.bataller.salmeron.chattrip.entities.Profile;
import es.sergio.bataller.salmeron.chattrip.fragments.AmigosFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.PostsFragment;
import es.sergio.bataller.salmeron.chattrip.fragments.ViajesFragment;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.chattrip.interfaces.DataWebService;
import es.sergio.bataller.salmeron.chattrip.utils.Constraints;
import es.sergio.bataller.salmeron.chattrip.utils.TabAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileNavFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileNavFragment extends Fragment {
    private LinearLayout linearLayout;
    private TextView txtTab1, txtTab2;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private List<Profile> listaProfile;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Context context;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProfileNavFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileNavFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileNavFragment newInstance(String param1, String param2) {
        ProfileNavFragment fragment = new ProfileNavFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile_nav, container, false);

        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = view.findViewById(R.id.chattrip_viewpager);
        tabLayout = view.findViewById(R.id.chattrip_tabLayout);
        collapsingToolbarLayout = view.findViewById(R.id.chattrip_collapsionToolbar);

        tabLayout.setupWithViewPager(viewPager);

        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragments(new AmigosFragment(), "FRIENDS");
        adapter.addFragments(new ViajesFragment(), "VIAJES");
        adapter.addFragments(new PostsFragment(), "POSTS");
        viewPager.setAdapter(adapter);

        initRecogeDatos();


    }

    private void TabView(TabLayout tab,String count,String name ,int posicion) {
        linearLayout = (LinearLayout)LayoutInflater.from(getContext()).inflate(R.layout.custom_tab_main,null);

        txtTab1=linearLayout.findViewById(R.id.tab_item_text1);
        txtTab2=linearLayout.findViewById(R.id.tab_item_text2);
        txtTab1.setText(name);
        txtTab2.setText(count);
        tab.getTabAt(posicion).setCustomView(linearLayout);

    }


    private void initRecogeDatos() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constraints.SERVER_SUCCESS_CODE) {
                    int i = 0;
                    Profile profile = (Profile) object;
                    if (profile.getFirst_name().isEmpty()) {
                        listaProfile.add((Profile) object);
                        if (listaProfile.isEmpty()) {
                            Toast.makeText(getContext(), "Mama la que hemos liado", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        collapsingToolbarLayout.setTitle(profile.getFirst_name());
                        TabView(tabLayout,String.valueOf(profile.getFrinds_count()),"FRIENDS",0);
                        TabView(tabLayout,"02","VIAJES",1);
                        TabView(tabLayout,"03","POSTS",1);
                    }

                }
            }
        });
    }
}