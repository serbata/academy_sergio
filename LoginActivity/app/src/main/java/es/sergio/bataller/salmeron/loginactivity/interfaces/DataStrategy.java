package es.sergio.bataller.salmeron.loginactivity.interfaces;

import java.util.List;

import es.sergio.bataller.salmeron.loginactivity.entities.Login;
import es.sergio.bataller.salmeron.loginactivity.entities.Profile;
import es.sergio.bataller.salmeron.loginactivity.entities.UserTrips;
import es.sergio.bataller.salmeron.loginactivity.utils.Pager;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public abstract class DataStrategy {
    public abstract Call<Token> refreshToken();


    public abstract void getFireBaseToken(InteractDispatcherGeneric interactDispatcherGeneric);

    public abstract void postLogin(Login login, InteractDispatcherObject interactDispatcherObject);

    public abstract void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject);

    public abstract void getMe(InteractDispatcherObject interactDispatcherObject);
    public abstract void getUserTrips(InteractDispatcherObject interactDispatcherObject);

    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface InteractDispatcherListObject<T> {
        void response(int code, List<T> object);
    }

    public interface InteractDispatcherGeneric {
        void response(int code, String message);
    }


    public interface ApiService {
        //REFRESH TOKEN
        @POST("auth/token/")
        Call<Token> refreshToken(@Body Login login);

        //GET FIREBASE TOKEN
        @GET("users/firebase_token/")
        Call<String> getFirebaseToken();

        //GET ME
        @GET("users/me/")
        Call<Profile> getMe();

        //LOGIN
        @POST("auth/token")
        Call<Token> postLogin(@Body Login login);

        //REGISTER
        @POST("auth/register/")
        Call<Profile> postRegister(@Body Profile register);

        //GET USER TRIPS
        @GET("trips/")
        Call<Pager<UserTrips>> getUserTrips();
    }
}
