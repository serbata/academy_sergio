package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import es.rudo.rudokotlinarchitecture.data.model.Medias
import java.io.Serializable

open class City : Serializable{
        var id: Int = 0
        var destination: Destination? = null
        var name: String? = null
        var start_date: String? =""
        var end_date: String?=""

    //    private var country: Country? = null

//    private var photoMetadata: PhotoMetadata? = null
}