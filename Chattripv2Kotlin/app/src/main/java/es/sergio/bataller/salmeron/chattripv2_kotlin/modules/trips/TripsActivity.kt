package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.trips

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AbsListView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.adapters.TripsPaginationAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityTripsBinding
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.home.HomeActivity

class TripsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTripsBinding
    private lateinit var viewModel: TripsViewModel
    private lateinit var layoutManager: LinearLayoutManager
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var isScrolling: Boolean = false
    private lateinit var listTrips: ArrayList<Any>
    private var page: Int = 1
    private lateinit var adapter: TripsPaginationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trips)
        viewModel = ViewModelProvider(this).get(TripsViewModel::class.java)
        binding.tripsViewModel = viewModel
        binding.tripsActivity = this
        layoutManager = binding.recyclerTrips.layoutManager as LinearLayoutManager




        adapter = TripsPaginationAdapter(object : TripsPaginationAdapter.TripListener {
            override fun onClick(cityId: Int) {
            }

            override fun onDeleted(cityId: Int) {
            }
        })


        initRecycler(adapter)



    }


    val scrollListener2 = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = binding.recyclerTrips.layoutManager as LinearLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount

            val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
            val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
            val isNotAtBeginning = firstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItemCount >= Constants.QUERRY_PAGE_SIZE
            val shouldPaginate = isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning &&
                    isTotalMoreThanVisible && isScrolling


            if (shouldPaginate) {
                sendDataPage()
                isScrolling = false
            } else {
                binding.recyclerTrips.setPadding(0, 0, 0, 0)
            }

        }
    }

    private fun initRecycler(
        adapter: TripsPaginationAdapter,
    ) {

        binding.recyclerTrips.apply {
            setAdapter(adapter)
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(scrollListener2)
        }
        adapter.addLoading()


    }

    private fun sendDataPage() {
        viewModel.getTripsApiCall()
        viewModel.cities.observe(this, Observer {
            it?.let {
                listTrips = it.results as ArrayList<Any>
                //   listTrips.addAll(it)
                if (listTrips.equals(adapter.currentList)) {
                    page++
                    viewModel.getTripsApiCall()
                } else {
                    adapter.submitList(listTrips)
                    adapter.notifyDataSetChanged()
                }

            }
        })

        isScrolling = false
    }

    private fun sendDataPageMenus() {
        viewModel.getTripsApiCall()
        viewModel.cities.observe(this, Observer {
            it?.let {
                listTrips = it.results as ArrayList<Any>

                adapter.submitList(listTrips)
            }
        })
        page--
        isScrolling = false
    }

    //  private fun setRecyclerViewScrollListener() {
    //        scrollListener = object : RecyclerView.OnScrollListener() {
    //          override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
    //              super.onScrollStateChanged(recyclerView, newState)
    //              val totalItemCount = recyclerView.layoutManager?.itemCount
    //              if (totalItemCount == layoutManager.findLastVisibleItemPosition() +1) {
    //                  binding.recyclerTrips.removeOnScrollListener(scrollListener)
    //              }
    //          }
    //      }
    //      binding.recyclerTrips.addOnScrollListener(scrollListener)
    //  }

    fun openFragments() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }
}