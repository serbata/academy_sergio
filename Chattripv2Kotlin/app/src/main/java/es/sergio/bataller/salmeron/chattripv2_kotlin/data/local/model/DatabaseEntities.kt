package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import androidx.room.Database
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DBLogin constructor(
    @PrimaryKey var id: Int = 0,
    var access_token: String,
    var client_id: String,
    var client_secret: String,
    var grant_type: String,
    var refresh_token: String,
    var username: String,
    var password: String
)

@Entity
data class DBUser(
    @PrimaryKey var id: Int,
    @PrimaryKey var name: String
)

@Entity
data class DBRegister constructor(
    @PrimaryKey var id: Int = 0,
    var email :String,
    var username:String,
    var password: String,
    var password_confirm:String
)



