package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityRegister2Binding
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.login.LoginActivity

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding:ActivityRegister2Binding
    private lateinit var viewModel: RegisterViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_register2) //Bind the view
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)


        binding.lifecycleOwner = this
        binding.registerViewModel = viewModel
        binding.registerActivity = this


        viewModel.eventCorrectRegister.observe(this, Observer { status ->
            status?.let {
                viewModel.eventCorrectRegister.value = null
                Toast.makeText(this,"Registro correcto", Toast.LENGTH_LONG).show()
                goLogin()
            }
        })


    }
    private fun goLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}



