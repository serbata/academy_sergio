package es.sergio.bataller.salmeron.chattripv2_kotlin.api

import es.rudo.rudokotlinarchitecture.data.model.User
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Login
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Register
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface Api {

    @GET("/rates")
    fun getRates(): Observable<List<String>>

    //LOGIN
    @POST("auth/token/")
    suspend fun postLogin(@Body login: Login): Response<Login>

    //GET ME
    @GET("users/me/")
    suspend fun getMe(): Response<User>


    //GET CITIES
    @GET("cities/")
    suspend fun getCities(@Query("page") page: Int): Response<Pager<City>>

    @POST("auth/register/")
    suspend fun postRegister(@Body register: Register): Response<Register>


}