package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import es.rudo.rudokotlinarchitecture.data.model.Medias
import java.io.Serializable

class Cities:Serializable {
    var id : Int?=0
    var name : String?=""
    var country: Country?=null
    var medias:Medias? = null
}