package es.sergio.bataller.salmeron.chattripv2_kotlin.helpers

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import java.lang.ref.WeakReference
import java.util.*

class NavigationHelper {

    companion object{
        const val F_HOME ="F_HOME"
        const val F_MAPS="F_MAPS"
        const val F_PROFILE="F_PROFILE"
    }

    private val stack = mutableListOf<WeakReference<Fragment>>()

    fun showFragment(
        activity: AppCompatActivity,
        currentFragment: Fragment?,
        futureFragment: Fragment?,
        tag: String
    ): Fragment {
        val fragmentManager = activity.supportFragmentManager
        val fragmentTransaction =fragmentManager.beginTransaction()

        if (tag == F_HOME  ||  tag== F_PROFILE || tag == F_MAPS) {
            clearStack()
            if (fragmentManager.findFragmentByTag(tag) != null) {
                fragmentTransaction.hide(currentFragment!!)
                fragmentTransaction.show(
                    Objects.requireNonNull(
                        fragmentManager.findFragmentByTag(
                            tag
                        )!!
                    )
                )
            } else {
                if (currentFragment != null) {
                    fragmentTransaction.hide(currentFragment)
                }
            }
        }else {
            fragmentTransaction.hide(currentFragment!!)
            stack.add(WeakReference(currentFragment))
        }
        return futureFragment!!
    }

    fun backStackFragment(activity: AppCompatActivity, currentFragment: Fragment?): Fragment? {

        val fragmentManager = activity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        var lastFragment: Fragment? = null

        if (stack.isEmpty()) {
            activity.finish()
        } else {
            lastFragment = fragmentManager.findFragmentByTag(stack[stack.size - 1].get()?.tag)
            fragmentTransaction.remove(currentFragment!!)
            fragmentTransaction.show(Objects.requireNonNull(lastFragment!!))
            fragmentTransaction.commit()
            stack.removeAt(stack.size - 1)
        }
        return lastFragment
    }

    private fun clearStack() {
        val iterator = stack.iterator()

        while (iterator.hasNext()) {
            var fragment = iterator.next().get()
            if (fragment != null)
                if (fragment.tag != null)
                    iterator.remove()
        }
    }
}