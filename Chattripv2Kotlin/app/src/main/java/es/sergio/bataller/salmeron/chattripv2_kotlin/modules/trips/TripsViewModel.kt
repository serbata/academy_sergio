package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.trips

import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.rudokotlinarchitecture.data.model.User
import es.sergio.bataller.salmeron.chattripv2_kotlin.adapters.TripsPaginationAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Pager
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.RetrofitClient
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class TripsViewModel : ViewModel() {
    private var page: Int= 1
    private var searchpage: Int =1
    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var cities = MutableLiveData<Pager<Any>>()

    //trips on click
    private var _navigateToTripDetail = MutableLiveData<Int>()
    val navigateToTripDetail: LiveData<Int>
        get() = _navigateToTripDetail


    init {
        getTripsApiCall()
    }

     fun getTrips(actualPage:Int) {
        var page = actualPage
        CoroutineScope(Dispatchers.IO).launch {
            val responseTrips = retrofitClient.getCities(actualPage)
            withContext(Dispatchers.Main) {
                if (responseTrips.code()==Constants.SERVER_SUCCESS_CODE) {
                    //Este funciona
                //   cities.value = responseTrips.body()?.results as ArrayList<Any>?
                //   cities.value?.let { cities.value?.addAll(it) }
                        // ESTO NO VA
                    //    var listTrips: ArrayList<Any>
                     //   listTrips = ArrayList()
                    //responseTrips.body()?.results?.let { listTrips.addAll(it) }

                    //cities.value?.add(responseTrips.body()?.results!!)
                    // responseTrips.body()?.results?.let { cities.value?.add(it) }

                    //Si falla algo y no se sabe que es el fallo estara aquí 100%
                    //Fallo a la hora de cargar los datos en el ArrayList<Any>, idk
                  //  cities.value?.add(listTrips)
                   // cities.value=cities.value
                    responseTrips.body()?.let { resultResponse ->
                    //   cities.value?.addAll(listOf(listOf(resultResponse.results)))
                      //  cities.value = resultResponse.results as ArrayList<Any>
                       // cities.postValue(resultResponse.results ?: 0)

                    }
                    //page++
                }
            }
        }
    }
    fun getTripsApiCall() {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                retrofitClient.getCities(page)
            },
            object : RetrofitClient.RemoteEmiter {
                override fun onResponse(response: Response<Any>) {
                    if (response.code() == Constants.SERVER_SUCCESS_CODE) {

                    cities.value = response.body() as Pager<Any>

                    }
                    page++
                }

                override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                    Log.e("Api errortype", errorType.toString())
                    Log.e("Api message", msg)
                }
            })
        }
    }


    private fun onTripClicked(id: Int) {
        _navigateToTripDetail.value = id
    }
}