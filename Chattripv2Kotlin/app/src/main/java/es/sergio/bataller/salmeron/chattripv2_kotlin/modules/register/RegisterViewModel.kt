package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.register

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import es.sergio.bataller.salmeron.chattripv2_kotlin.App
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.RetrofitClient
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.AppDatabase
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.DBRegister
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Register
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityRegister2Binding
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Utils
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.extensions.isValidEmail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class RegisterViewModel : ViewModel() {
    private val retrofitCLient: RetrofitClient = RetrofitClient()

    //Variables para los EditText
    private val name = MutableLiveData<String>()
    private val email = MutableLiveData<String>()
    private val password = MutableLiveData<String>()
    private val passwordConfirm = MutableLiveData<String>()

    //Variables para los errores de campos
    private val nameError = MutableLiveData<String>()
    private val emailError = MutableLiveData<String>()
    private val passwordError = MutableLiveData<String>()
    private val passwordConfirmErro = MutableLiveData<String>()

    //Error Register
    var eventErrorRegister = MutableLiveData<Boolean>()

    //Correct Register
    var eventCorrectRegister = MutableLiveData<Boolean>()


    fun validationRegister(){
        name.value.let {
            if (it.isNullOrBlank() && it.isNullOrEmpty()){
                eventErrorRegister.value = true
                nameError.value ="Este campo no puede estar vacio"

            }
            if (it?.matches(Constants.EMAIL_PATTERN.toRegex()) == true){
                eventErrorRegister.value = true
               nameError.value = "Esto no es un email"
            }
            if (it?.length?.equals(0..8) == true){
                eventErrorRegister.value=true
                nameError.value ="El usuario no debe contener mas de 8 caracteres"
            }
            else{
                eventCorrectRegister.value=true
            }
        }
        email.value.let {
            if (it.isNullOrBlank() && it.isNullOrEmpty()) {
                eventErrorRegister.value=true
                emailError.value ="Este campo no puede estar vacio"
            }
            if (!it?.matches(Constants.EMAIL_PATTERN.toRegex())!!){
                eventErrorRegister.value=true
                emailError.value = "Debe ser un email"
            }else{
                eventCorrectRegister.value=true
            }
        }
        password.value.let {
            if (it.isNullOrEmpty()&&it.isNullOrBlank()){
                eventErrorRegister.value=true
                passwordError.value = "No debe estar vacio"
            }
            if ((it == passwordConfirm.value).not()){
                eventErrorRegister.value=true
                passwordError.value ="Las contraseñas no coinciden"
            }
            else{
                eventCorrectRegister.value=true
            }
        }
        passwordConfirm.value.let {
            if (it.isNullOrBlank() && it.isNullOrEmpty()){
                eventErrorRegister.value = true
                passwordConfirmErro.value = "No debe estar vacio"
            }
            else{
                eventCorrectRegister.value = true
            }
        }
    }








}