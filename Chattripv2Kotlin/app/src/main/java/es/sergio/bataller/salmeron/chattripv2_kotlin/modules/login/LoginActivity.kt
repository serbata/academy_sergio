package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.sergio.bataller.salmeron.chattripv2_kotlin.MainActivity
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityLogin2Binding
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.register.RegisterActivity
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.trips.TripsActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLogin2Binding
    private lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login_2) //Bind the view
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        //Specify the current activity as the lifecircle
        //This is used,the binding can observe LiveData updates
        binding.lifecycleOwner = this

        //Set the viewModel for dataBinding
        //to all the data in the ViewModel
        binding.loginViewModel = viewModel

        viewModel.eventLoginError.observe(this, Observer<Boolean> { hasError ->
            if (hasError) showLoginError()
        })
        viewModel.passwordError.observe(this,Observer<String> { error ->
        binding.inputPasswordLogin.error=error

            })

        viewModel.usernameError.observe(this, Observer<String> { error ->
            binding.inputUsernameLogin.error= error
        })

        viewModel.eventLoginCorrect.observe(this, Observer<Boolean> { isSuccessful ->
            if (isSuccessful){
                openTrips()
            }
        })

        binding.loginTextRegister.setOnClickListener(View.OnClickListener {
            openRegister()
        })

    }

    private fun openTrips() {
        val intent = Intent(this, TripsActivity::class.java)
        startActivity(intent)
    }
    private fun openRegister(){
        val intent = Intent(this,RegisterActivity::class.java)
        startActivity(intent)
    }
    private fun openMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    private fun showLoginError() {
        Toast.makeText(applicationContext, "Login error!", Toast.LENGTH_LONG).show()
    }
}