package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import java.io.Serializable

class UserTrips : Serializable {
    var id: String? = ""
    var destination: Destination? = null
    var start_date: String? = ""
    var end_date: String? = ""
}