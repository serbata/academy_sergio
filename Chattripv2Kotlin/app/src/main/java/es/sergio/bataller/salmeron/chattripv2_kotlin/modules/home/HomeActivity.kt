package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import es.rudo.rudokotlinarchitecture.data.model.User
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.adapters.ViewPagerAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityHomeBinding
import kotlinx.android.synthetic.main.custom_tab_home.view.*

class HomeActivity : AppCompatActivity() {
    private val adapter by lazy { ViewPagerAdapter(this) }
    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: HomeViewModel
    private var myUser = MutableLiveData<User>()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_home)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        binding.lifecycleOwner = this
        binding.homeViewModel = viewModel

        viewModel.user.observe(this, Observer<User> { userresult ->
            myUser.value =userresult
            initPage(myUser)
        })
        binding.homeViewpager2.adapter = adapter




        val tabLayoutMediator = TabLayoutMediator(binding.homeTablayout, binding.homeViewpager2,
                        TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                            when(position){
                                0->{
                                    tab.setCustomView(R.layout.custom_tab_home)
                                    tab.view.custom_tab_text1.text="AMIGOS"
                                    tab.view.custom_tab_text2.text="02"
                                }
                                1->{
                                    tab.setCustomView(R.layout.custom_tab_home)
                                    tab.view.custom_tab_text1.text="VIAJES"
                                    tab.view.custom_tab_text2.text="02"
                                }
                                2->{
                                    tab.setCustomView(R.layout.custom_tab_home)
                                    tab.view.custom_tab_text1.text="POSTS"
                                    tab.view.custom_tab_text2.text="02"
                                }
                            }
                        })
        tabLayoutMediator.attach()
    }

    private fun initPage(user: MutableLiveData<User>) {
        viewModel.user.observe(this, Observer {
            it?.let {
                myUser.value=  it
            }
        })
        binding.homeCollapsingToolbar.title =myUser.value?.first_name

        binding.homeTextBio.text = user.value?.bio

    }




}