package es.rudo.rudokotlinarchitecture.api

import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Api
import java.io.IOException

import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import kotlin.jvm.Throws

class AccessTokenAuthenticator : Authenticator {

    @Throws(IOException::class)
    override fun authenticate(route: Route?, response: Response): Request? {
//        val accessToken = App.preferences.getAccessToken()
        if (!isRequestWithAccessToken(response) || "accesstoken" == null) {
            return null
        }
        synchronized(this) {
            //            val newAccessToken = App.preferences.getAccessToken()
//            // Access token is refreshed in another thread.
//            if (accessToken != newAccessToken) {
//                return newRequestWithAccessToken(response.request(), newAccessToken)
//            }
//
//            // Need to refresh an access token
            lateinit var api: Api
            var call = api.getRates()
//
////            val res = call.execute()
////            val data = res.body()
//            if (data != null) {
//                val updatedAccessToken = data!!.getAccess_token()
//                val updatedRefreshToken = data!!.getRefresh_token()
////                App.preferences.setAccessToken(updatedAccessToken)
////                App.preferences.setRefreshToken(updatedRefreshToken)
//                return newRequestWithAccessToken(response.request(), updatedAccessToken)
//            } else {
//                return newRequestWithAccessToken(
//                    response.request(),
//                    App.preferences.getAccessToken()
//                )
//                //                return null;
        }
        return null
    }

    private fun isRequestWithAccessToken(response: Response): Boolean {
        val header = response.request.header("Authorization")
        return header != null && header.startsWith("Bearer")
    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}