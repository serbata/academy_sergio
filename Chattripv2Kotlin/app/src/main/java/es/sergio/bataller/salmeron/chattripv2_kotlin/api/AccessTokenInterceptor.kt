package es.rudo.rudokotlinarchitecture.api

import es.sergio.bataller.salmeron.chattripv2_kotlin.App
import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import kotlin.jvm.Throws

class AccessTokenInterceptor : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = App.preferences.getAccessToken()
        val request = newRequestWithAccessToken(chain.request(), accessToken.toString())
        return chain.proceed(request)
    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}