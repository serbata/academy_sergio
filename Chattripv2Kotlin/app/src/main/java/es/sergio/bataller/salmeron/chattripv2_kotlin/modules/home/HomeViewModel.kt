package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.rudokotlinarchitecture.data.model.Medias
import es.rudo.rudokotlinarchitecture.data.model.User
import es.sergio.bataller.salmeron.chattripv2_kotlin.App
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.RetrofitClient
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityHomeBinding
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Utils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class HomeViewModel: ViewModel() {

    val name = MutableLiveData<String>()
    var user = MutableLiveData<User>()
    val lastname = MutableLiveData<String>()
    val bio = MutableLiveData<String>()
    val country = MutableLiveData<String>()
    val image = MutableLiveData<Medias>()

    private val retrofitClient: RetrofitClient = RetrofitClient()


    init {
        getMeApiCall()

    }



    private fun getMeApiCall() {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                retrofitClient.getMe()
            },
                object : RetrofitClient.RemoteEmiter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {

                            user.value = response.body() as User?
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}