package es.sergio.bataller.salmeron.chattripv2_kotlin.helpers

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(val context: Context) {
    private val PREF_FILE = "MyPreferences"
    private val ACCESS_TOKEN = "access_token"
    private val REFRESH_TOKEN = "refresh_token"
    private val USER_ID = "user_id"
    private val FIREBASE_ID = "firebase_id"

    private fun getSharedPreferences(): SharedPreferences? {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
    }

    fun getAccessToken(): String? {
        return getSharedPreferences()?.getString(ACCESS_TOKEN, null)
    }

    fun setAccessToken(accessToken: String?) {
        this.getSharedPreferences()?.edit()?.putString(ACCESS_TOKEN, accessToken)?.apply()
    }

    fun getRefreshToken(): String? {
        return getSharedPreferences()?.getString(REFRESH_TOKEN, null)
    }

    fun setRefreshToken(refreshToken: String?) {
        this.getSharedPreferences()?.edit()?.putString(REFRESH_TOKEN, refreshToken)?.apply()
    }

    fun getUserId(): String? {
        return getSharedPreferences()?.getString(USER_ID, null)
    }

    fun setUserId(userId: String?) {
        this.getSharedPreferences()?.edit()?.putString(USER_ID, userId)?.apply()
    }

}