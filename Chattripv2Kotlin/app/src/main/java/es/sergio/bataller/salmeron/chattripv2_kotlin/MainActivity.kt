package es.sergio.bataller.salmeron.chattripv2_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.GoogleMap
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ActivityMainBinding
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.NavigationHelper


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var map: GoogleMap

    private val navigation: NavigationHelper = NavigationHelper()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.lifecycleOwner = this


        /**
         *

        val homeFragment = HomeFragment()
        val profileFragment = ProfileFragment()
        val mapsFragment = DiaryMapFragment()

       // createMapFragment()
        makeCurrentFragment(homeFragment, profileFragment, "F_HOME")
        binding.mainBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.ic_home -> makeCurrentFragment(homeFragment, homeFragment, "F_HOME")
                R.id.ic_maps -> makeCurrentFragment(mapsFragment, mapsFragment, "F_MAPS")
                R.id.ic_profile -> makeCurrentFragment(
                    profileFragment,
                    profileFragment,
                    "F_PROFILE"
                )
            }
            true
        }
         */

    }


    private fun makeCurrentFragment(fragment: Fragment, futureFragment: Fragment, tag: String) {
        navigation.showFragment(this, fragment, futureFragment, tag)
    }

    private fun createMapFragment() {
        /**
         *

        val mapFragment = supportFragmentManager
        .findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        }

        override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
        map = googleMap
        }
        }
         */
    }
}