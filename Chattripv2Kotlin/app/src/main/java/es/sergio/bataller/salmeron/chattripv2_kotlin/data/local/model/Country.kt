package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import java.io.Serializable

class Country : Serializable{
    var name : String? =""
    var code : String?=""
}