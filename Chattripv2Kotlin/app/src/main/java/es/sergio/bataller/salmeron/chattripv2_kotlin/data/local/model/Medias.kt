package es.rudo.rudokotlinarchitecture.data.model

import java.io.Serializable

class Medias : Serializable{
    var file: String? = null
    var thumbnail: String? = null
    var midsize: String? = null
    var fullsize: String? = null

}