package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.adapters.TripsAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.adapters.TripsPaginationAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Pager
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.FragmentTripsBinding

 class TripsFragmentsv2 : Fragment() {
    private var page: Int = 1
    private lateinit var binding: FragmentTripsBinding
    private lateinit var viewModel: TripsFragmentViewModel

    var cities = MutableLiveData<ArrayList<Any>>()
    private lateinit var citi: MutableLiveData<ArrayList<Any>>
    private lateinit var user_trips:MutableLiveData<Pager<City>>
    private var layoutManager: RecyclerView.LayoutManager? = null
     private lateinit var adapter : TripsAdapter
    private lateinit var adapter2: TripsPaginationAdapter
    private var pagination: Int = 1


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_trips,
            container,
            false
        )
        /**
         *

        adapter2 = TripsPaginationAdapter(object : TripsPaginationAdapter.TripListener {
            override fun onClick(cityId: Int) {
                Toast.makeText(binding.root.context, "Trip Seleccionado", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onDeleted(cityId: Int) {
                Toast.makeText(binding.root.context, "Trips DELETE", Toast.LENGTH_LONG).show()
            }

        })

         */

        viewModel = ViewModelProvider(this).get(TripsFragmentViewModel::class.java)
        binding.lifecycleOwner = this
        binding.tripsViewModel = viewModel

        binding.recyclerTrips.adapter = adapter
        binding.recyclerTrips.layoutManager = LinearLayoutManager(context)
        //adapter2.addLoading()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.getCities(pagination)
        viewModel.cities.observe(viewLifecycleOwner, Observer {
            user_trips.value = it
            val trip : List<City> = it.results as List<City>
            adapter = TripsAdapter(trip)
        })
    }



}