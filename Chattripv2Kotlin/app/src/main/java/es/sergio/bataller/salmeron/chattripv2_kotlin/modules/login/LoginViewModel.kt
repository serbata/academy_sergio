package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.login

import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Room
import es.rudo.rudokotlinarchitecture.data.model.User
import es.sergio.bataller.salmeron.chattripv2_kotlin.App
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Config
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.RetrofitClient
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.AppDatabase
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.DBLogin
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Login
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.AppPreferences
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Utils
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.extensions.isValidEmail
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.trips.TripsActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginViewModel : ViewModel() {
    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    var myUser = MutableLiveData<User>()
    private var userLoged: Login = Login()

    //Error Login
 var eventLoginError = MutableLiveData<Boolean>()


    //Correct Login
    var eventLoginCorrect = MutableLiveData<Boolean>()


    fun postLogin() {
        val login = Login()
        login.client_id = Config.CLIENT_ID
        login.client_secret = Config.CLIENT_SECRET
        login.grant_type = Config.GRANT_TYPE_LOGIN
        login.username = username.value
        login.password = password.value

        /**
         *

        if (Utils.isNetworkAvailable(App.instance)){
            CoroutineScope(Dispatchers.IO).launch {
                RetrofitClient().apiCall({
                    RetrofitClient().postLogin(login)
                },
                object : RetrofitClient.RemoteEmiter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code()==Constants.SERVER_SUCCESS_CODE){
                        userLoged = response.body() as Login
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype",errorType.toString())
                        Log.e("Api message" ,msg)
                    }
                })
            }
        }
         */

        if (Utils.isNetworkAvailable(App.instance)) {
            CoroutineScope(Dispatchers.IO).launch {
                val responseLogin: Response<Login> = retrofitClient.postLogin(login)
                if (responseLogin.code() == Constants.SERVER_SUCCESS_CODE) {
                    val db = Room.databaseBuilder(
                        App.instance,
                        AppDatabase::class.java,"app-database.db"
                    ).build()
                    var dbLogin = DBLogin(1,"","","","","","","")
                    dbLogin!!.access_token = responseLogin.body()?.access_token.toString()
                    db.loginDao().insert(dbLogin)
                    db.loginDao().getLogin()

                    App.preferences.setAccessToken(responseLogin.body()?.access_token)
                    App.preferences.setRefreshToken(responseLogin.body()?.refresh_token)

                    getMeApiCall()
                }
            }
        }else {
            Toast.makeText(App.instance,"No network availeble", Toast.LENGTH_LONG).show()
        }
    }


        private fun getMe(){
            if (Utils.isNetworkAvailable(App.instance)){
                CoroutineScope(Dispatchers.IO).launch {
                    val responseMe = retrofitClient.getMe()
                    if (responseMe.code() == Constants.SERVER_SUCCESS_CODE) {
                        withContext(Dispatchers.Main) {
                            App.preferences.setUserId(responseMe.body()?.id)
                            eventLoginCorrect.value=true
                            myUser.value=responseMe.body()

                        }
                    }
                }
            }
        }
    private fun getMeApiCall() {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                retrofitClient.getMe()
            },
                object : RetrofitClient.RemoteEmiter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            eventLoginCorrect.value = true
                            myUser.value = response.body() as User?
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
    fun checkLogin() {
        eventLoginError.value = false
        if (username.value?.isEmpty()!!) {
            eventLoginError.value = true
            usernameError.value = "Este campo no puede ser vacío"
        } else if (!username.value!!.isValidEmail()) {
            eventLoginError.value = true
            usernameError.value = "Formato de émail no válido"
        }
        if (password.value.toString().isEmpty()) {
            eventLoginError.value = true
            passwordError.value = "Este campo no puede ser vacío"
        }
        if (!eventLoginError.value!!) {
            postLogin()
        }
    }

}