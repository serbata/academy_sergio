package es.sergio.bataller.salmeron.chattripv2_kotlin.helpers

object Constants {

    val TAG_ONE = "first"
    val TAG_SECOND = "second"
    val TAG_THIRD = "third"
    val TAG_FOURTH = "fourth"
    val MAX_HISTORIC = 5

    val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    val SHORT_DATE_PATTERN = "dd MMM"
    val MEDIUM_DATE_PATTERN = "dd MMM yyyy"
    val SERVER_DATE_PATTERN = "yyyy-MM-dd"
    val SERVER_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    val FILTER_MALE = "M"
    val FILTER_FEMALE = "F"
    val FILTER_ALL_GENDER = ""


    val FORMAT_VIDEO = ".mp4"
    val FORMAT_PHOTO = ".jpg"

    val TYPE_ALBUM = "album"
    val TYPE_MEDIA = "media"

    val SERVER_SUCCESS_CODE = 200
    val SERVER_CREATED_CODE = 201
    val SERVER_NOCONTENT_CODE = 204
    val SERVER_BADREQUEST_CODE = 400
    val SERVER_UNAUTHORIZED_CODE = 401
    val SERVER_FORBIDDEN_CODE = 403
    val SERVER_NOTFOUND_CODE = 404
    val SERVER_TIMEOUT_CODE = 408
    val SERVER_INTERNALSERVER_CODE = 500

    val QUERRY_PAGE_SIZE = 15

}