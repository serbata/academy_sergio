package es.sergio.bataller.salmeron.chattripv2_kotlin.modules.fragments

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Pager
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.RetrofitClient
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class TripsFragmentViewModel: ViewModel() {
    private lateinit var viewModel : TripsFragmentViewModel

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var trips : MutableLiveData<ArrayList<Any>> = MutableLiveData()
    var cities = MutableLiveData<Pager<City>>()

    init {
    getCities(page = 1)

    }

     fun getCities( page : Int) {

        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                retrofitClient.getCities(page)
            },
            object :RetrofitClient.RemoteEmiter{
                override fun onResponse(response: Response<Any>) {
                    if (response.code() == Constants.SERVER_SUCCESS_CODE){
                        val responseTrips : Pager<City> = response.body() as Pager<City>
                       cities.value = responseTrips
                        cities.value!!.results = responseTrips.results

                    }

                }

                override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                    Log.e("Api errortype", errorType.toString())
                    Log.e("Api message", msg)
                }

            })
        }
    }
}