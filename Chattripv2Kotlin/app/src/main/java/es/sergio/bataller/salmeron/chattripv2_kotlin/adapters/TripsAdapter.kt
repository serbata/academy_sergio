package es.sergio.bataller.salmeron.chattripv2_kotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import es.sergio.bataller.salmeron.chattripv2_kotlin.R
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City

class TripsAdapter (val city: List<City>) : RecyclerView.Adapter<TripsAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = city[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return city.size
    }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textCity: TextView = itemView.findViewById(R.id.text_city)
        private val imageCity: ImageView = itemView.findViewById(R.id.image_background)

        fun bind(item: City) {
            val context = itemView.context.resources

            textCity.text = item.name
            Glide.with(itemView)
                .load(item.destination?.medias?.get(0)?.midsize)
                .into(imageCity)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.item_trip, parent, false)

                return ViewHolder(view)
            }
        }
    }

}