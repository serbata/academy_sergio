package es.sergio.bataller.salmeron.chattripv2_kotlin.api

class Pager<T> {
    val count: Int = 0
    val next: String? = null
    val previus: String? = null
    var results: List<T>? = null

}