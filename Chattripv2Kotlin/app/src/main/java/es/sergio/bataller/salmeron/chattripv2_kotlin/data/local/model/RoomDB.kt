/*
* Copyright (C) 2019 Google Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import android.app.Person
import android.content.Context
import androidx.room.*

@Database(
    entities = [DBLogin::class, DBUser::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun loginDao(): LoginDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "app-database.db"
        )
            .build()
    }
}

@Dao
interface LoginDao {
    @Query("select * from DBLogin")
    fun getLogin(): DBLogin

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(login: DBLogin?)
}

@Dao
interface UserDao {
    @Query("select * from DBUser")
    fun getMe(): DBUser
}


        //Ejemplo de ROOM
@Dao
interface PruebaDao {
    @Query("select *  from DBRegister")
    fun getRegister(): Register

    @Query("SELECT * FROM DBUser WHERE id = :id")
    fun getById(id: Int): Register
    //@Update para actualizar
}