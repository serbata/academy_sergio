package es.sergio.bataller.salmeron.chattripv2_kotlin.api

import android.util.Log
import androidx.databinding.library.BuildConfig
import com.google.gson.GsonBuilder
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import es.rudo.rudokotlinarchitecture.api.AccessTokenInterceptor
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Login
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Register
import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.Protocol
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit


    class RetrofitClient {
            enum class ErrorType{
                HTTP_EXCEPTION,
                NETWORK,
                TIMEOUT,
                UNKNOWN
            }
        interface RemoteEmiter {
            fun onResponse(response: Response<Any>)
            fun onError(errorType: ErrorType, msg: String)
        }

        private val clientWithAuth by lazy {
            Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(
                    OkHttpClient()
                        .newBuilder()
                        .connectTimeout(15, TimeUnit.SECONDS)
                        .writeTimeout(15,TimeUnit.SECONDS)
                        .readTimeout(15,TimeUnit.SECONDS)
                        .connectionPool(ConnectionPool(0,5,TimeUnit.MINUTES))
                        .protocols(listOf(Protocol.HTTP_1_1))
                        .addInterceptor(AccessTokenInterceptor())
                        .addInterceptor(
                            LoggingInterceptor.Builder()
                                .setLevel(Level.BODY)
                                .request("Request")
                                .response("Response")
                                .addHeader("Accept", "application/json")
                                .build()
                        )
                        .build()
                )
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())) //Moshi
                .build().create(Api::class.java)
        }

        private val clientWithoutAuth by lazy {
            Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(
                    OkHttpClient()
                        .newBuilder()
                        .addInterceptor(
                            LoggingInterceptor.Builder()
                                .setLevel(Level.BODY)
                                .request("Request")
                                .response("Response")
                                .addHeader("Accept", "application/json")
                                .build()
                        )
                        .build()
                )
    //            .addCallAdapterFactory(CoroutineCallAdapterFactory()) //Deprecated
                .addConverterFactory(MoshiConverterFactory.create()) //Moshi
    //            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())) // Gson
                .build().create(Api::class.java)
        }


        suspend inline fun <T> apiCall(
            crossinline responseFunction: suspend () -> T,
            emitter: RemoteEmiter
        ) {
            try {
                val response = withContext(Dispatchers.IO) { responseFunction.invoke() }
                withContext(Dispatchers.Main) {
                    emitter.onResponse(response as Response<Any>)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                    Log.e("ApiCalls", "Call error: ${e.localizedMessage}", e.cause)
                    when (e) {
                        is HttpException -> {
                            val body = e.response()?.errorBody().toString()
                            emitter.onError(ErrorType.HTTP_EXCEPTION, body)
                        }
                        is SocketTimeoutException -> emitter.onError(
                            ErrorType.TIMEOUT,
                            "Timeout Error"
                        )
                        is IOException -> emitter.onError(ErrorType.NETWORK, "Thread Error")
                        else -> emitter.onError(ErrorType.UNKNOWN, "Unknown Error")
                    }
                }
            }
        }


        suspend fun postLogin(login: Login) = clientWithoutAuth.postLogin(login)
        suspend fun getMe() = clientWithAuth.getMe()
        suspend fun getCities(page: Int) = clientWithAuth.getCities(page)

        suspend fun postRegister(register: Register) = clientWithoutAuth.postRegister(register)

    }