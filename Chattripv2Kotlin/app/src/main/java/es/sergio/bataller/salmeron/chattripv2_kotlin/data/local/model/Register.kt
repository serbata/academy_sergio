package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import java.io.Serializable

class Register :Serializable{
    var email:String? = null
    var username:String? = null
    var password:String?=null
     var password_confirm:String?=null
}