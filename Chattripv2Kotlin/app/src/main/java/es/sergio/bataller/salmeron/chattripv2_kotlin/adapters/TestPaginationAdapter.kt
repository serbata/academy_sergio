package es.sergio.bataller.salmeron.chattripv2_kotlin.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Loader
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ItemLoadingBinding
import es.sergio.bataller.salmeron.chattripv2_kotlin.databinding.ItemTripBinding


private const val VIEW_TYPE_LOADING = 0
private const val VIEW_TYPE_NORMAL = 1
private var isLoaderVisible = false

class TestPaginationAdapter(private val clickListener: TripListenerv2) :
    ListAdapter<Any, RecyclerView.ViewHolder>(TripsPaginationAdapterCallback()) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolderLoading -> {
                getItem(position) as Loader
                holder.bind()
            }
            is ViewHolder -> {
                val item = getItem(position) as City
                holder.bind(item, clickListener)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_LOADING -> ViewHolderLoading.from(parent)
            VIEW_TYPE_NORMAL -> ViewHolder.from(parent)
            else -> ViewHolder.from(parent)
        }
    }

    class ViewHolder private constructor(val binding: ItemTripBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: City, clickListener: TripListenerv2) {
            binding.city = item
            binding.executePendingBindings()

            binding.buttonItem.setOnClickListener {
                clickListener.onClick(item.id)
            }

            Glide.with(itemView).load(item.destination?.medias?.get(0)?.midsize)
                .into(binding.imageBackground)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemTripBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ViewHolderLoading private constructor(val binding: ItemLoadingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {

        }

        companion object {
            fun from(parent: ViewGroup): ViewHolderLoading {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemLoadingBinding.inflate(layoutInflater, parent, false)
                return ViewHolderLoading(binding)
            }
        }
    }

    fun addLoading() {
        isLoaderVisible = true
        val currentData = ArrayList<Any>()
        currentData.addAll(currentList)

        currentData.add(Loader())
        submitList(currentData)
        notifyItemInserted(currentData.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val currentData = ArrayList<Any>()
        currentData.addAll(currentList)

        val position: Int = currentData.size - 1
        val item: Loader? = getItem(position) as Loader?
        if (item != null) {
            currentData.removeAt(position)
            submitList(currentData)
            notifyItemRemoved(position)
        }
    }

    class TripsPaginationAdapterCallback : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            return oldItem == newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            return oldItem == newItem
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is City -> VIEW_TYPE_NORMAL
            is Loader -> VIEW_TYPE_LOADING
            else -> VIEW_TYPE_NORMAL
        }
    }

    interface TripListenerv2 {
        fun onClick(cityId: Int)
        fun onDeleted(cityId: Int)
    }

}