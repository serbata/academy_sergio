package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import es.rudo.rudokotlinarchitecture.data.model.Medias
import java.io.Serializable

class Destination: Serializable {
    var id : Int? = null
    var name : String? =""
    var contry: Country? =null
    var medias : List<Medias>? =null

}
