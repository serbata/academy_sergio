package es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model

import androidx.room.PrimaryKey
import es.sergio.bataller.salmeron.chattripv2_kotlin.api.Config
import java.io.Serializable

class Login : Serializable {
    @PrimaryKey var id: Int =0
    var access_token: String? = null
    var client_id = Config.CLIENT_ID
    var client_secret = Config.CLIENT_SECRET
    var grant_type: String? = null
    var refresh_token: String? = null
    var username: String? = null
    var password: String? = null
}