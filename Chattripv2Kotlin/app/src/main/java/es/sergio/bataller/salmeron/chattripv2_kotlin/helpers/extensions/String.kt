package es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.extensions

import es.sergio.bataller.salmeron.chattripv2_kotlin.helpers.Constants


fun String.isValidEmail() = matches(Constants.EMAIL_PATTERN.toRegex())