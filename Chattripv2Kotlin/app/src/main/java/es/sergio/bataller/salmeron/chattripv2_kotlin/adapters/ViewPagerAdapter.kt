package es.sergio.bataller.salmeron.chattripv2_kotlin.adapters


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.fragments.FriendsFragment
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.fragments.HomeFragment
import es.sergio.bataller.salmeron.chattripv2_kotlin.modules.fragments.TripsFragmentsv2


class ViewPagerAdapter(fa: FragmentActivity): FragmentStateAdapter(fa) {

    companion object{
        private const val ARG_OBJECT = "object"
    }

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> { HomeFragment()}
            1 -> { TripsFragmentsv2() }
            2 -> {FriendsFragment()}
            else -> HomeFragment()
        }
    }


}