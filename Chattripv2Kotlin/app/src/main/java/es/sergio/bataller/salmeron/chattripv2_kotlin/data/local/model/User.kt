package es.rudo.rudokotlinarchitecture.data.model

import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.City
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.Country
import es.sergio.bataller.salmeron.chattripv2_kotlin.data.local.model.UserTrips
import java.io.Serializable

class User : Serializable {
    var id: String? = ""
    var username: String? = ""
    var first_name: String? = ""
    var last_name: String? = ""
    var bio: String? = ""
    var birth_date: String? = ""
    var gender: String? = ""
    var city: String? = ""
    var country: String? = ""
    var medias: List<Medias>? = null
    var email: String? = ""
    var show_city: Boolean? = null
    var firebase_id: String? = ""
    var language: String? = ""
    var blocked_users: Array<String>? = null
    var is_friend: String? = ""
    var friends_count: Int? = null
    var social_register: Boolean? = null
    var is_disabled_by_admin: Boolean? = null
    var instagram_link: String? = ""

}