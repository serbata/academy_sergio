package es.rudo.androiduibootcamp.Exercise07;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import es.rudo.androiduibootcamp.R;

public class Exercise07Activity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemReselectedListener {

    private Fragment fragment = null;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise07);
        /**
         *
         MenuInflater menuinflater = getMenuInflater();
         menuinflater.inflate(R.menu.main_menu,menu);
         */
        initMenu();


    }

    private void initMenu() {
        loadFragment(new HomeFragment());
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemReselectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_home:
                fragment = new AddFragment();
                break;
            case R.id.action_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.action_search:
                fragment = new SearchFragment();
                break;
            case R.id.comments:
                fragment = new HomeFragment();
                break;

        }
        loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    /**
     *  private void loadFragment(Fragment fragment) {
     *         // load fragment
     *         FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
     *         transaction.replace(R.id.frame_container, fragment);
     *         transaction.addToBackStack(null);
     *         transaction.commit();
     *     }
     * @param fragment
     */


}
