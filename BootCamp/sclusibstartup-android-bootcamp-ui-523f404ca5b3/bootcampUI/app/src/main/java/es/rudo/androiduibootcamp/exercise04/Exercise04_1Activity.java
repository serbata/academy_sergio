package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import es.rudo.androiduibootcamp.R;

public class Exercise04_1Activity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnNombre;
    private int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_1);
        initVariables();
        initListener();
    }

    private void initListener() {
        btnNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!txtNombre.getText().toString().isEmpty()) {
                    Intent intent = new Intent(Exercise04_1Activity.this, Exercise04_2Activity.class);
                    intent.putExtra("NOMBRE", txtNombre.getText().toString());
                    startActivity(intent);

                }
            }
        });
    }

    private void initVariables() {
        txtNombre = findViewById(R.id.edit_name);
        btnNombre = findViewById(R.id.btnNombre);
    }
}
