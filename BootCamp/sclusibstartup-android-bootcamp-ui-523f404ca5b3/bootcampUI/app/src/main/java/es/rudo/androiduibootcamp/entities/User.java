package es.rudo.androiduibootcamp.entities;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private int id;
    private String nombre;
    private String email;
    private String foto;
    private boolean seguido = false;

    public User() {
    }

    public User(int id, String nombre, String email, String foto, boolean seguido) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.foto = foto;
        this.seguido = seguido;
    }

    protected User(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        email = in.readString();
        foto = in.readString();
        seguido = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(nombre);
        parcel.writeString(email);
        parcel.writeString(foto);
        parcel.writeByte((byte) (seguido ? 1 : 0));
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public boolean isSeguido() {
        return seguido;
    }

    public void setSeguido(boolean seguido) {
        this.seguido = seguido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

