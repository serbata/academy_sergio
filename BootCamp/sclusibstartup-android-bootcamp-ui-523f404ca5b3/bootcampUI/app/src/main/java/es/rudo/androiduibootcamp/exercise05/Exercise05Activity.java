package es.rudo.androiduibootcamp.exercise05;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import es.rudo.androiduibootcamp.Exercise07.AddFragment;
import es.rudo.androiduibootcamp.Exercise07.HomeFragment;
import es.rudo.androiduibootcamp.Exercise07.ProfileFragment;
import es.rudo.androiduibootcamp.Exercise07.SearchFragment;
import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.adapters.Exercise05Adapter;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Activity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemReselectedListener {
    private ArrayList<User> listaUsers;
    private Exercise05Adapter adapter;
    private int filaUsers;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private Button button_personas, button_grupos;
    private RelativeLayout notifationCount1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise05);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        button_personas = findViewById(R.id.buttom_personas);
        button_grupos = findViewById(R.id.buttom_grupos);
        notifationCount1 = findViewById(R.id.relative_layout_count1);
        button_grupos.hasFocus();
        button_personas.hasFocus();

        initAdapter();
        initRecycler();
        inicializarDatos();
        initMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ex05_menu, menu);

        MenuItem searchViewItem = menu.findItem(R.id.app_bar_search);
        //  MenuItem item1 = menu.findItem(R.id.notify);
        //  MenuItemCompat.setActionView(item1,R.layout.notification_update_count_layout);
        //  notifationCount1 = (RelativeLayout) MenuItemCompat.getActionView(item1);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setInputType(InputType.TYPE_CLASS_TEXT);
        // searchView.setBackgroundColor(Color.WHITE);
        searchView.setBackgroundResource(R.drawable.bg_white_rounded);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        ArrayList<User> filteredlist = new ArrayList<>();

        // running a for loop to compare elements.
        for (User item : listaUsers) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getNombre().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            // at last we are passing that filtered
            // list to our adapter class.
            adapter.filterList(filteredlist);
        }
    }

    private void initRecycler() {
        //RECYCLER VIEW
        linearLayoutManager = new LinearLayoutManager(this);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new Exercise05Adapter(this, filaUsers, listaUsers);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));


    }

    private void initAdapter() {
        //Inicializacion del adapter, Array y demases...
        listaUsers = new ArrayList<>();
        filaUsers = R.layout.item_search_user;
        recyclerView = findViewById(R.id.recycler_users);
        adapter = new Exercise05Adapter(this, filaUsers, listaUsers);
    }

    private void inicializarDatos() {
        //Añadimos Datos a nuestros Users
        for (int i = 0; i < 4; i++) {
            listaUsers.add(new User(i, "Pepe", "edt1@trd", "https://www.trecebits.com/wp-content/uploads/2019/02/Persona-1-445x445.jpg", false));
        }
        for (int i = 0; i < 5; i++) {
            listaUsers.add(new User(i, "Juan", "edt122@trd", "https://www.trecebits.com/wp-content/uploads/2019/02/Persona-1-445x445.jpg", false));
        }
        listaUsers.add(new User(22, "Silvia", "edt122@trd", "https://www.trecebits.com/wp-content/uploads/2019/02/Persona-1-445x445.jpg", false));
    }

    private void initMenu() {
        BottomNavigationView navigation = findViewById(R.id.navigation2);
        navigation.setOnNavigationItemReselectedListener(this);
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

    }

}

