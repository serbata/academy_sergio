


    /**
     *
     * @param exercise05Activity
     * @param plantillaEx
     * @param listaUsers

    public Exercise05Adapter(Exercise05Activity exercise05Activity, int plantillaEx, ArrayList<User> listaUsers){
        super(exercise05Activity,plantillaEx,listaUsers);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View fila = LayoutInflater.from(context).inflate(resource,null);
        User user = objects.get(position);

         final Button btnSeguido = fila.findViewById(R.id.btn_seguido);
         final Button btnSeguir = fila.findViewById(R.id.button_follow);
        TextView txtNombre = fila.findViewById(R.id.text_username);
        ImageView imgUser = fila.findViewById(R.id.image_user);

        if (user.getNombre().isEmpty()){
            txtNombre.setText(user.getEmail());
        }else {
        txtNombre.setText(user.getNombre());
        }
        if (!(imgUser == null)){
            Picasso.get()
                    .load(user.getFoto())
                    .placeholder(R.drawable.logo_rudo_horizontal)
                    .error(R.drawable.baseline_warning_black_18dp)
                    .fit()
                    .into(imgUser);
        }
        else {
            imgUser.setVisibility(View.GONE);
        }
        btnSeguir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                user.setSeguido(true);
                btnSeguir.setVisibility(View.GONE);
                btnSeguido.setVisibility(View.VISIBLE);

            }
        });
btnSeguido.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        User user = new User();
        user.setSeguido(false);
        btnSeguido.setVisibility(View.GONE);
        btnSeguir.setVisibility(View.VISIBLE);
    }
});
return fila;
    }
     */