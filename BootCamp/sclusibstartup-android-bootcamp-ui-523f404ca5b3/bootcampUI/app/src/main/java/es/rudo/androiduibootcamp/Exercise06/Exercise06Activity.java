package es.rudo.androiduibootcamp.Exercise06;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class Exercise06Activity extends AppCompatActivity {
    private TextView txtNombre, txtCheckBox, txtSwitch;
    private EditText edit_nombre;
    private CheckBox checkBox06;
    private Switch aSwitch;
    private Button btnGuardar, btnBorrar, btn_verPeople;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise06);


        initVariable();
        DeleteCache();
        SaveDataCache();
        ReeadDataCache();
    }

    private void SaveDataCache() {
        //Guardar datos en la cache
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edit_nombre.getText().toString().isEmpty()) {
                    //Almacenar en las SharedPreferences
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("nombre", edit_nombre.getText().toString());
                    //editor.putString("checkbox",checkBox.toString());
                    if (aSwitch.isChecked()) {
                        editor.putString("swich", "Esta chekeado");
                    } else {
                        editor.putString("swich", "No esta chekeado");
                    }
                    if (checkBox06.isChecked()) {
                        editor.putString("checkbox", "ChekBox Esta CHEKED");
                    } else {
                        editor.putString("checkbox", "ChekBox No esta CHEKED");
                    }
                    editor.commit();
                }
            }
        });
    }

    private void ReeadDataCache() {
        //Inicializamos las SharedPreferences
        //Leer datos de la cache
        sharedPreferences = this.getSharedPreferences(Configuracion.SP_Ex6, MODE_PRIVATE);
        String nombre = sharedPreferences.getString("nombre", null);
        String checkBox = sharedPreferences.getString("checkbox", null);
        String Swich = sharedPreferences.getString("swich", null);
        //Pintar datos recogidos
        if (nombre == null &&
                checkBox == null &&
                Swich == null) {
            txtNombre.setText("");
            txtSwitch.setText("");
            txtCheckBox.setText("");
        } else {
            txtNombre.setText(nombre);
            txtSwitch.setText(Swich);
            txtCheckBox.setText(checkBox);

            if (Swich.equals("Esta chekeado")) {
                aSwitch.setChecked(true);
            } else {
                aSwitch.setChecked(false);
            }
            edit_nombre.setText(nombre);
            if (checkBox.equals("ChekBox Esta CHEKED")) {
                checkBox06.setChecked(true);
            } else {
                checkBox06.setChecked(false);
            }
        }
    }

    private void DeleteCache() {
        //Borrar datos de la cache
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNombre.setText("");
                txtCheckBox.setText("");
                txtSwitch.setText("");
                aSwitch.setChecked(false);
                edit_nombre.setText("");
            }
        });
        btn_verPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise06Activity.this, Exercise05Activity.class);
                startActivity(intent);
            }
        });
    }

    private void initVariable() {
        txtNombre = findViewById(R.id.txtNombre);
        txtCheckBox = findViewById(R.id.txtCheckBox);
        txtSwitch = findViewById(R.id.txtSwitch);
        edit_nombre = findViewById(R.id.edittext_nombre);
        checkBox06 = findViewById(R.id.checbox_destroyer);
        aSwitch = findViewById(R.id.swAutorizado);
        btnGuardar = findViewById(R.id.btnGuardar06);
        btnBorrar = findViewById(R.id.btnBorrar06);
        btn_verPeople = findViewById(R.id.button_verPeople);
    }
}
