package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Exercise04_2Activity extends AppCompatActivity {
    private EditText txtApellidos;
    private Button btnApellidos;
    private TextView txtNombre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_2);

        initVariables();
        initIntent();
        initListener();
    }

    private void initListener() {
        btnApellidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                if (!txtApellidos.getText().toString().isEmpty()) {
                    Bundle bundle= new Bundle();
                    intent.putExtra("APELLIDOS", txtApellidos.getText().toString());
                    String datos = bundle.getString("NOMBRE");
                    intent.putExtra("NOMBRE",datos);
                    intent = new Intent(Exercise04_2Activity.this, Exercise04_3Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Exercise04_2Activity.this, "Tienes que rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initIntent() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        String Datos = bundle.getString("NOMBRE");
        txtNombre.setText(Datos);
    }

    private void initVariables() {
        txtApellidos = findViewById(R.id.edit_surname);
        txtNombre = findViewById(R.id.text_name);
        btnApellidos = findViewById(R.id.btnApellidos);
    }
}
