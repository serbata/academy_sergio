package es.rudo.androiduibootcamp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Adapter extends RecyclerView.Adapter<Exercise05Adapter.UserVH> {
    private Context context;
    private int resource;
    private List<User> objects;
    private Picasso picasso;

    public Exercise05Adapter(@NonNull Context context, int resource, @NonNull ArrayList<User> objects) {
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public UserVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View usersItem = LayoutInflater.from(context).inflate(resource, null);
        usersItem.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        return new UserVH(usersItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserVH holder, int i) {
        User user = objects.get(i);
        if (user.getNombre().isEmpty() || user.getNombre() == null) {
            holder.txtUsername.setText(user.getEmail());
        } else {
            holder.txtUsername.setText(user.getNombre());
        }
        if (user.getFoto() == null || user.getFoto().isEmpty()) {
            holder.imgUser.setImageResource(R.drawable.logo_rudo_horizontal);
        } else {
            Picasso.get()
                    .load(user.getFoto())
                    .error(R.drawable.baseline_warning_black_18dp)
                    .into(holder.imgUser);
        }


        holder.btnSeguir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                user.setSeguido(true);
                holder.btnSeguir.setVisibility(View.GONE);
                holder.btnSeguido.setVisibility(View.VISIBLE);

            }
        });
        holder.btnSeguido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                user.setSeguido(false);
                holder.btnSeguido.setVisibility(View.GONE);
                holder.btnSeguir.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return objects.size();
    }

    public static class UserVH extends RecyclerView.ViewHolder {
        TextView txtUsername;
        ImageView imgUser;
        View item_search;
        Button btnSeguido, btnSeguir;

        public UserVH(@NonNull View usersItem) {
            super(usersItem);
            txtUsername = usersItem.findViewById(R.id.text_username);
            imgUser = usersItem.findViewById(R.id.imgV_User);
            item_search = usersItem;
            btnSeguido = usersItem.findViewById(R.id.btn_seguido);
            btnSeguir = usersItem.findViewById(R.id.button_follow);
        }
    }

    // method for filtering our recyclerview items.
    public void filterList(ArrayList<User> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        objects = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }

    /**
     * Antiguo filtro +Illnes
     * @return public Filter getFilter() {
    return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {

    @Override protected FilterResults performFiltering(CharSequence charSequence) {
    List<User> filteredList = new ArrayList<>();
    if (charSequence == null || charSequence.length() == 0) {
    filteredList.addAll(filteredList);
    } else {
    String filterPatern = charSequence.toString().toLowerCase().trim();
    for (User user : objects) {
    if (user.getNombre().toLowerCase().contains(filterPatern)) {
    filteredList.add(user);
    }
    }
    }
    FilterResults results = new FilterResults();
    results.values = filteredList;
    notifyDataSetChanged();
    return results;
    }

    @Override protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
    objects.clear();
    objects.addAll((List) filterResults.values);
    notifyDataSetChanged();
    }
    };
     */
}
