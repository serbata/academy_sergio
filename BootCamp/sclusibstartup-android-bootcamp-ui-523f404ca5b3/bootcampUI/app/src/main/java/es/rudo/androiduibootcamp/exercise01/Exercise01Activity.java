package es.rudo.androiduibootcamp.exercise01;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import es.rudo.androiduibootcamp.Exercise06.Exercise06Activity;
import es.rudo.androiduibootcamp.Exercise07.Exercise07Activity;
import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.exercise02.Excercise02Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_1Activity;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class Exercise01Activity extends AppCompatActivity {
    private final int CODE_EXERCISE02_NAV = 01;
    private TextView txtEj7;
    private Button btnEjercicio5, btnEjercicio06, btnActividad04, button_activity07;
    private ImageView imgRudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise01);
        InitVariables();
        initListener();

    }


    private void InitVariables() {
        txtEj7 = findViewById(R.id.txtEj7);
        btnEjercicio5 = findViewById(R.id.btnActivity5);
        btnEjercicio06 = findViewById(R.id.btn_activity06);
        btnActividad04 = findViewById(R.id.btn_Actividad4);
        imgRudo = findViewById(R.id.imgRudo);
        button_activity07 = findViewById(R.id.btn_activity07);
    }

    private void initListener() {
        imgRudo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise01Activity.this, Excercise02Activity.class);
                startActivityForResult(intent, CODE_EXERCISE02_NAV);
            }
        });

        btnActividad04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise01Activity.this, Exercise04_1Activity.class);
                startActivity(intent);
            }
        });
        btnEjercicio5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Exercise05Activity.class);
                startActivity(intent);
            }
        });
        btnEjercicio06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Exercise06Activity.class);
                startActivity(intent);
            }
        });
        button_activity07.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise01Activity.this, Exercise07Activity.class);
                startActivity(intent);
            }
        });
    }


}
