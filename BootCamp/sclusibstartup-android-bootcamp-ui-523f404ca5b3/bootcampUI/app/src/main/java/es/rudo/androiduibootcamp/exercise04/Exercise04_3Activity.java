package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Exercise04_3Activity extends AppCompatActivity {

    private TextView txtFullName;
    private Button btnFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_3);
        initVariables();
        initInent();
        initListener();

    }

    private void initListener() {
        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initInent() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        String nombre = (String) bundle.getString("NOMBRE");
        String apellidos = (String) bundle.getString("APELLIDOS");
        if (nombre.isEmpty() && apellidos.isEmpty()) {
            Toast.makeText(this, "Hubo un error y los datos estan vacios", Toast.LENGTH_SHORT).show();
        } else {
            txtFullName.setText(nombre + " " + apellidos);
        }
    }

    private void initVariables() {
        txtFullName = findViewById(R.id.text_fullname);
        btnFinalizar = findViewById(R.id.btnFinalizar);
    }
}
