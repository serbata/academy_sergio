package es.rudo.androiduibootcamp.exercise02;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.Exercise06.Exercise06Activity;
import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.exercise03.Exercise03Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_1Activity;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class Excercise02Activity extends AppCompatActivity {
    private TextInputEditText txtUsername, txtPassword;
    private Button btnLogin;
    private TextView txtForgottenPassword, txtNewAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise02);
        InitVariables();
        InitListener();
    }


    private void InitListener() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtUsername.getText().toString().isEmpty() &&
                        !txtPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Excercise02Activity.this, "Se ha iniciado sesion", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Excercise02Activity.this, Exercise06Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Excercise02Activity.this, "Revise todos los campos, faltan por rellenar", Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Excercise02Activity.this, Exercise03Activity.class);
                startActivity(intent);
            }
        });
        txtForgottenPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Excercise02Activity.this, "Enseguida le mandamos un email", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void InitVariables() {
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPasword);
        txtForgottenPassword = findViewById(R.id.txtForgottenPasword);
        txtNewAccount = findViewById(R.id.txtCrearCuenta);
        btnLogin = findViewById(R.id.btnLogin);
    }


}
