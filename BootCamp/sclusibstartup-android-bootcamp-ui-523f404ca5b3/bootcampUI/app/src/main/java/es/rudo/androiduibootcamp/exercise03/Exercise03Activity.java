package es.rudo.androiduibootcamp.exercise03;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Exercise03Activity extends AppCompatActivity {
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private EditText txtNombre, txtEmail, txtContraseña, txtContraseña2;
    private Button btnRegistrarse;
    private CheckBox CBTerminos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise03);

        initVariables();
        initListener();
    }

    private void initVariables() {
        txtNombre = findViewById(R.id.add_nombre);
        txtEmail = findViewById(R.id.add_emailv2);
        txtContraseña = findViewById(R.id.add_passwordv2);
        txtContraseña2 = findViewById(R.id.add_password2);
        btnRegistrarse = findViewById(R.id.btnRegistrarse);
        CBTerminos = findViewById(R.id.checkBox);
    }

    private void initListener() {
        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (!hasWindowFocus()) {
                    String mail = txtEmail.getText().toString();
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                        txtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.baseline_warning_black_18dp, 0);
                    } else {
                        Log.i("yay!", "Email is valid!!!");
                        txtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.baseline_done_black_18dp, 0);
                    }
                }
            }
        });

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtEmail.setError(null);
                if (txtNombre.getText().toString().isEmpty()) {
                    txtNombre.setError("Falta rellenar datos");
                } else if (txtNombre.getText().toString().trim().matches(emailPattern)) {
                    txtNombre.setError("Esto no es un email, CAMBIALO PORFAVOR");
                }
                if (txtEmail.getText().toString().isEmpty()) {
                    txtEmail.setError("Falta rellenar el Email");
                    txtEmail.requestFocus();
                } else if (txtEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Exercise03Activity.this, "Email valido", Toast.LENGTH_SHORT).show();
                } else {
                    txtEmail.setError("Introduce un email valido");
                    txtEmail.requestFocus();
                }
                if (!txtContraseña.getText().toString().isEmpty() &&
                        !txtContraseña2.getText().toString().isEmpty()) {
                    if (txtContraseña.getText().toString().equals(txtContraseña2.getText().toString())) {
                        Toast.makeText(Exercise03Activity.this, "Muy Bien Sabes Poner 2 contraseñas IGUALES!", Toast.LENGTH_SHORT).show();
                    } else {
                        txtContraseña.setError("No coinciden las contraseñas");
                        txtContraseña2.setError("No coinciden las contraseñas");
                        txtContraseña.requestFocus();
                        txtContraseña2.requestFocus();
                    }
                }
                if (CBTerminos.isChecked()) {
                    Toast.makeText(Exercise03Activity.this, "Gracias por confiar en nosotros", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Exercise03Activity.this, "Tienes que confirmar los terminos y condiciones de la página", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
