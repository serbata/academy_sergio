<?xml version="1.0" encoding="utf-8"?>
<layout>

    <data>

        <variable
            name="${fragmentLowercase}ViewModel"
            type="${escapeKotlinIdentifiers(packageName)}.${fragmentClass}ViewModel" />

        <variable
            name="${fragmentLowercase}Fragment"
            type="${escapeKotlinIdentifiers(packageName)}.${fragmentClass}Fragment" />

    </data>

	<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    xmlns:tools="http://schemas.android.com/tools"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent"
	    tools:context="${packageName}.${fragmentClass}Fragment">

	</androidx.constraintlayout.widget.ConstraintLayout>

</layout>