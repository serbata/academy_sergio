package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider


class ${className}Activity : AppCompatActivity() {

    private lateinit var binding: Activity${className}Binding
    private lateinit var viewModel: ${className}ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_${classNameLowercase})
        viewModel = ViewModelProvider(this).get(${className}ViewModel::class.java)

        binding.lifecycleOwner = this
        binding.${classNameLowercase}Activity = this
        binding.${classNameLowercase}ViewModel = viewModel

        initObservers()
    }

    private fun initObservers() {
    
    }
}