<manifest xmlns:android="http://schemas.android.com/apk/res/android">

    <application>
        <activity android:name="${packageName}.${className}Activity"
            android:screenOrientation="portrait"/>
    </application>
</manifest>