#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

#parse("File Header.java")
class ${NAME} : Fragment() {


    private lateinit var binding: Fragment${NAME_uppercase}Binding
    private lateinit var viewModel: ${NAME_uppercase}ViewModel

    companion object {
        fun newInstance(): Fragment {
            return ${NAME_uppercase}Fragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_${NAME_lowercase}, container, false)
        viewModel = ViewModelProvider(this).get(${NAME_uppercase}ViewModel::class.java)

        val view = binding.root
        binding.lifecycleOwner = this
        binding.${NAME_lowercase}Fragment = this
        binding.${NAME_lowercase}ViewModel = viewModel

        initObservers()

        return view
    }
    
    private fun initObservers() {
    
    }
}