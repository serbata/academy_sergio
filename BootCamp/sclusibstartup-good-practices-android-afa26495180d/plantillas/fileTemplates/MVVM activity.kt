#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider

#parse("File Header.java")
class ${NAME} : AppCompatActivity() {

    private lateinit var binding: Activity${NAME_uppercase}Binding
    private lateinit var viewModel: ${NAME_uppercase}ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_${NAME_lowercase})
        viewModel = ViewModelProvider(this).get(${NAME_uppercase}ViewModel::class.java)

        binding.lifecycleOwner = this
        binding.${NAME_lowercase}Activity = this
        binding.${NAME_lowercase}ViewModel = viewModel

        initObservers()
    }

    private fun initObservers() {
    
    }
}