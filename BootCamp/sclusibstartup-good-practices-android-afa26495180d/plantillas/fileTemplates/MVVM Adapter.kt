#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ${NAME}(
    private val context: Context,
    private val clickListener: ${OBJECT_NAME}ClickListener
) :
    ListAdapter<${OBJECT_NAME}, ${NAME}.ViewHolder>(ListAdapterCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, context, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: Item${OBJECT_NAME}Binding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: ${OBJECT_NAME},
            context: Context,
            clickListener: ${OBJECT_NAME}ClickListener
        ) {
            binding.${OBJECT_LOWERCASE} = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = Item${OBJECT_NAME}Binding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ListAdapterCallback : DiffUtil.ItemCallback<${OBJECT_NAME}>() {
        override fun areItemsTheSame(oldItem: ${OBJECT_NAME}, newItem: ${OBJECT_NAME}): Boolean {
            return oldItem.name == newItem.name
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: ${OBJECT_NAME}, newItem: ${OBJECT_NAME}): Boolean {
            return oldItem == newItem
        }
    }

    interface ${OBJECT_NAME}ClickListener {
        fun onClick(item: ${OBJECT_NAME})
    }
}