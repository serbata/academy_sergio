package es.rudo.kotlinofficialgoodpractices.data.model

import java.io.Serializable

class CartItem : Serializable {
    var id: String? = null
    var name: String? = null
    var price: Double = 0.0
    var quantity: Int = 0
}