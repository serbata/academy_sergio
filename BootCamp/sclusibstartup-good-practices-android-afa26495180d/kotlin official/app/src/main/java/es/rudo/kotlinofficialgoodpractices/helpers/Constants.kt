package es.rudo.kotlinofficialgoodpractices.helpers

object Constants {

    //REGEXP PATTERNS
    const val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z._-]+\\.+[a-z]+"
    const val PASSWORD_PATTERN_STRICT =
        "((?=.*\\d)(?=.*[a-zñáéíóú])(?=.*[A-ZÑÁÉÍÓÚ])(?=.*\\p{Punct}).{6,20})"

    //DATE PATTERNS
    const val SHORT_DATE_PATTERN = "dd MMM"
    const val MEDIUM_DATE_PATTERN = "dd/MM/yyyy"
    const val SERVER_DATE_PATTERN = "yyyy-MM-dd"
    const val SERVER_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
    const val SERVER_USER_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
    const val IMAGE_DATETIME_PATTERN = "yyyyMMdd_HHmmss"

    const val HOUR_PATTERN = "HH:mm"
    const val LONG_HOUR_PATTERN = "HH:mm:ss"
    const val LONG_DATETIME_PATTERN = "dd/MM/yyyy • hh:mm aa"

    //WEBSERVICES CODES
    const val SERVER_SUCCESS_CODE = 200
    const val SERVER_CREATED_CODE = 201
    const val SERVER_NOCONTENT_CODE = 204
    const val SERVER_BADREQUEST_CODE = 400
    const val SERVER_UNAUTHORIZED_CODE = 401
    const val SERVER_FORBIDDEN_CODE = 403
    const val SERVER_NOTFOUND_CODE = 404
    const val SERVER_TIMEOUT_CODE = 408
    const val SERVER_INTERNALSERVER_CODE = 500
    const val SERVER_BANNED_CODE = 403

    //BIOMETRIC CODES
    const val BIOMETRIC_UNAVAILABLE = 0
    const val BIOMETRIC_AVAILABLE = 1
    const val BIOMETRIC_NOT_REGISTERED = 2

    //DRAWABLE COMPOUNDS POSITIONS
    const val DRAWABLE_START = 0
    const val DRAWABLE_TOP = 1
    const val DRAWABLE_END = 2
    const val DRAWABLE_BOTTOM = 3

    //STRIPE
    const val STRIPE_PUBLISHABLE_KEY =
        "pk_test_51H7cWzLn6u8SAHDtKmA0SAMmGbVq3ev58KkaSpHfldwNcLf08LSyJu6eUyP9L4p4DZg4O1YKX5jc1LXJYyhELTaN00lIplJ4jP"
}