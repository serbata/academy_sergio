package es.rudo.kotlinofficialgoodpractices.api

import com.google.gson.JsonObject
import es.rudo.kotlinofficialgoodpractices.data.model.Cart
import es.rudo.kotlinofficialgoodpractices.data.model.CartItem
import es.rudo.kotlinofficialgoodpractices.data.model.StripeIntent
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {

    //STRIPE
    @POST("payments/ephemeral_key/")
    suspend fun createEphemeralKey(@Body stripeIntent: StripeIntent): Response<JsonObject>

    @POST("payments/create_payment/")
    suspend fun createPaymentIntent(@Body cart: Cart): Response<StripeIntent>

    @POST("payments/confirm_payment/")
    suspend fun confirmPayment(@Body stripeIntent: StripeIntent): Response<String>

    @GET("payments/items/")
    suspend fun getItems(): Response<List<CartItem>>

}