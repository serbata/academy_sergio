package es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.adapters.CartAdapter
import es.rudo.kotlinofficialgoodpractices.data.model.CartItem
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityCartBinding


class CartActivity : AppCompatActivity() {

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var binding: ActivityCartBinding
    private lateinit var viewModel: CartViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cart)
        viewModel = ViewModelProvider(this).get(CartViewModel::class.java)

        binding.lifecycleOwner = this
        binding.cartActivity = this
        binding.cartViewModel = viewModel

        initAdapter()
        initObservers()
    }

    private fun initObservers() {
        viewModel.isCartEmpty.observe(this, Observer {
            it.let {
                if (it) {
                    binding.fabCart.visibility = View.GONE
                } else {
                    binding.fabCart.visibility = View.VISIBLE
                }

            }
        })
    }

    fun openCart() {
        startActivity(
            Intent(applicationContext, CheckoutActivity::class.java).putExtra(
                "Cart",
                viewModel.cart
            )
        )
    }

    private fun initAdapter() {
        layoutManager = binding.recyclerItems.layoutManager as LinearLayoutManager

        val adapter = CartAdapter(
            applicationContext,
            object : CartAdapter.CartItemClickListener {
                override fun onClick(item: CartItem) {
                    val itemSearch = viewModel.cart.items.find {
                        it.id == item.id
                    }
                    if (item.quantity == 0) {
                        viewModel.cart.items.remove(item)
                    } else {
                        if (itemSearch != null) {
                            viewModel.cart.items[viewModel.cart.items.indexOfFirst { it.id == item.id }] =
                                item
                        } else {
                            viewModel.cart.items.add(item)
                        }
                    }
                    viewModel.countCart()
                }

            }
        )
        binding.recyclerItems.adapter = adapter

        viewModel.itemList.observe(this,
            {
                it.let {
                    adapter.submitList(it)
                    adapter.notifyDataSetChanged()
                }
            })
    }
}