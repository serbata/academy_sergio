package es.rudo.kotlinofficialgoodpractices.modules.shimmer_effect

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.adapters.CardAdapter
import es.rudo.kotlinofficialgoodpractices.data.model.Card
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityShimmerEffectBinding

class ShimmerEffectActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShimmerEffectBinding
    private lateinit var viewModel: ShimmerEffectViewModel

    private val cartList = listOf(
            Card().apply {
                title = "Lorem ipsum"
                subtitle = "Lorem ipsum dolor sit amet"
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                image = "https://picsum.photos/id/866/536/354"
            },
            Card().apply {
                title = "Lorem ipsum"
                subtitle = "Lorem ipsum dolor sit amet"
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                image = "https://picsum.photos/id/866/536/354"
            },
            Card().apply {
                title = "Lorem ipsum"
                subtitle = "Lorem ipsum dolor sit amet"
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                image = "https://picsum.photos/id/866/536/354"
            },
            Card().apply {
                title = "Lorem ipsum"
                subtitle = "Lorem ipsum dolor sit amet"
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
                image = "https://picsum.photos/id/866/536/354"
            }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shimmer_effect)
        viewModel = ViewModelProvider(this).get(ShimmerEffectViewModel::class.java)

        binding.lifecycleOwner = this
        binding.activity = this
        binding.viewModel = viewModel
    }

    override fun onResume() {
        super.onResume()
        startShimmer()
        initAdapters()
    }

    override fun onPause() {
        stopShimmer()
        super.onPause()
    }


    private fun initAdapters() {
        binding.recycler.apply {
            adapter = CardAdapter(this@ShimmerEffectActivity, object : CardAdapter.CardClickListener {
                override fun onClick(item: Card) {
                    Toast.makeText(this@ShimmerEffectActivity, "Card clicked!", Toast.LENGTH_SHORT).show()
                }
            }).apply {
                simulateDelay()
                submitList(cartList)
            }

            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun simulateDelay() = Handler(mainLooper).postDelayed({
        stopShimmer()
    }, 2000)

    private fun startShimmer() = binding.shimmerViewContainer.apply {
        binding.recycler.visibility = View.GONE
        startShimmer()
        visibility = View.VISIBLE
    }

    private fun stopShimmer() = binding.shimmerViewContainer.apply {
        visibility = View.GONE
        stopShimmer()
        binding.recycler.visibility = View.VISIBLE
    }
}