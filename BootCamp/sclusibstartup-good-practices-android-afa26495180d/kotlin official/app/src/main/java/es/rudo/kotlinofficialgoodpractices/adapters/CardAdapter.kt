package es.rudo.kotlinofficialgoodpractices.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListAdapter

import com.bumptech.glide.Glide
import es.rudo.kotlinofficialgoodpractices.data.model.Card
import es.rudo.kotlinofficialgoodpractices.databinding.ItemCardBinding

class CardAdapter(
    private val context: Context,
    private val clickListener: CardClickListener
) :
    ListAdapter<Card, CardAdapter.ViewHolder>(ListAdapterCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, context, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ItemCardBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Card,
            context: Context,
            clickListener: CardClickListener
        ) {
            binding.card = item
            binding.clickListener = clickListener
            binding.executePendingBindings()

            binding.title.text = item.title
            binding.subtitle.text = item.subtitle
            binding.description.text = item.description

            Glide.with(context).load(item.image)
                .centerInside()
                .into(binding.image)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemCardBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ListAdapterCallback : DiffUtil.ItemCallback<Card>() {
        override fun areItemsTheSame(oldItem: Card, newItem: Card): Boolean {
            return oldItem.id == newItem.id
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Card, newItem: Card): Boolean {
            return oldItem == newItem
        }
    }

    interface CardClickListener {
        fun onClick(item: Card)
    }
}