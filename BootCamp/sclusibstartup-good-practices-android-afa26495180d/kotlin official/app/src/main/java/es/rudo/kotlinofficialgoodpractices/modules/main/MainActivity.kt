package es.rudo.kotlinofficialgoodpractices.modules.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityMainBinding
import es.rudo.kotlinofficialgoodpractices.modules.biometrics.BiometricsActivity
import es.rudo.kotlinofficialgoodpractices.modules.capture_security.CaptureSecurityActivity
import es.rudo.kotlinofficialgoodpractices.modules.exoplayer.VideoActivity
import es.rudo.kotlinofficialgoodpractices.modules.shimmer_effect.ShimmerEffectActivity
import es.rudo.kotlinofficialgoodpractices.modules.signature.SignatureActivity
import es.rudo.kotlinofficialgoodpractices.modules.stripe.PayActivity
import es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced.CartActivity


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.lifecycleOwner = this
        binding.mainActivity = this
        binding.mainViewModel = viewModel

        initObservers()
    }

    private fun initObservers() {

    }

    fun goToStripe() {
        startActivity(Intent(this, PayActivity::class.java))
    }

    fun goToStripe2() {
        startActivity(Intent(this, CartActivity::class.java))
    }

    fun goToBiometrics() {
        startActivity(Intent(this, BiometricsActivity::class.java))
    }

    fun goToVideoPlayer() {
        startActivity(Intent(this, VideoActivity::class.java))
    }

    fun goToRecordSecurity() {
        startActivity(Intent(this, CaptureSecurityActivity::class.java))
    }

    fun goToSignature() {
        startActivity(Intent(this, SignatureActivity::class.java))
    }

    fun goToShimmer() {
        startActivity(Intent(this, ShimmerEffectActivity::class.java))
    }
}