package es.rudo.kotlinofficialgoodpractices.data.model

import java.io.Serializable

class Card : Serializable {
    var id: String? = null
    var title: String? = null
    var subtitle: String? = null
    var image: String? = null
    var description: String? = null
}