package es.rudo.rudokotlinarchitecture.modules.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import es.rudo.rudokotlinarchitecture.R
import es.rudo.rudokotlinarchitecture.databinding.FragmentFeedBinding

class FeedFragment : Fragment() {


    companion object {
        fun newInstance() = FeedFragment()
    }

    private lateinit var viewModel: FeedViewModel
    private lateinit var binding: FragmentFeedBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_feed,
            container,
            false
        )

        Toast.makeText(context, "TEST", Toast.LENGTH_LONG).show()

        // Get the viewModel
        viewModel = ViewModelProviders.of(this).get(FeedViewModel::class.java)

        return binding.root
    }

}
