package es.rudo.rudokotlinarchitecture.data.model

data class MyName(var name: String = "", var nickname: String = "")