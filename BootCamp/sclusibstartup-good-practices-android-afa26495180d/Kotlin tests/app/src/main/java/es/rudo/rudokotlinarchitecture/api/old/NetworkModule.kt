package es.rudo.rudokotlinarchitecture.api.old

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import dagger.Module
import dagger.Provides
import dagger.Reusable
import es.rudo.rudokotlinarchitecture.BuildConfig
import es.rudo.rudokotlinarchitecture.api.AccessTokenAuthenticator
import es.rudo.rudokotlinarchitecture.api.AccessTokenInterceptor
import es.rudo.rudokotlinarchitecture.api.Api
import es.rudo.rudokotlinarchitecture.api.Config.API_URL
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@Suppress("unused")

object NetworkModule {
    /**
     * Provides the Post service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Post service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_URL)
            .client(
                OkHttpClient()
                    .newBuilder()
                    .authenticator(AccessTokenAuthenticator())
                    .addInterceptor(AccessTokenInterceptor())
                    .addInterceptor(
                        LoggingInterceptor.Builder()
                            .loggable(BuildConfig.DEBUG)
                            .setLevel(Level.NONE)
                            .request("Request")
                            .response("Response")
                            .addHeader("Accept", "application/json")
                            .build()
                    )
                    .build()
            )
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}