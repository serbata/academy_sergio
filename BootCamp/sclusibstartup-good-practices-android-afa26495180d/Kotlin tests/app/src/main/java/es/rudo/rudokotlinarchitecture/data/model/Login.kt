package es.rudo.rudokotlinarchitecture.data.model

import androidx.room.PrimaryKey
import java.io.Serializable

import es.rudo.rudokotlinarchitecture.api.Config

class Login : Serializable {
    @PrimaryKey var id: Int = 0
    var access_token: String? = null
    var client_id = Config.CLIENT_ID
    var client_secret = Config.CLIENT_SECRET
    var grant_type: String? = null
    var refresh_token: String? = null
    var username: String? = null
    var password: String? = null




}