package es.rudo.rudokotlinarchitecture.modules.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import es.rudo.rudokotlinarchitecture.R
import es.rudo.rudokotlinarchitecture.databinding.ActivityLoginBinding
import es.rudo.rudokotlinarchitecture.modules.trips.TripsActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewmodel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login) //Binds the view
        viewmodel =
            ViewModelProviders.of(this).get(LoginViewModel::class.java) // Declares the viewmodel

        // Specify the current activity as the lifecycle owner of the binding.
        // This is used so that the binding can observe LiveData updates
        binding.lifecycleOwner = this

//        // Set the viewmodel for databinding - this allows the bound layout access
//        // to all the data in the VieWModel
        binding.loginViewModel = viewmodel


        viewmodel.eventLoginError.observe(this, Observer<Boolean> { hasError ->
            if (hasError) showLoginError()
        })

        viewmodel.passwordError.observe(this, Observer<String> { error ->

            input_password.error = error
        })

        viewmodel.usernameError.observe(this, Observer<String> { error ->
            input_username.error = error
        })

        viewmodel.eventLoginCorrect.observe(this, Observer<Boolean> { isSuccessful ->
            if (isSuccessful){
                openTrips()
            }
        })

//        initListeners()
    }

    /**
     * Esto sería una alternativa a bindear la acción desde el xml al viewmodel
     */
//    fun initListeners() {
//        binding.buttonLogin.setOnClickListener {
//            viewmodel.checkLogin()
//        }
//    }

    private fun openTrips() {
        val intent = Intent(this, TripsActivity::class.java)
        startActivity(intent)
//        finish()
    }

    private fun showLoginError() {
        Toast.makeText(applicationContext, "Login error!", Toast.LENGTH_LONG).show()
    }
}
