package es.rudo.rudokotlinarchitecture

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import es.rudo.rudokotlinarchitecture.databinding.ActivityMainBinding
import es.rudo.rudokotlinarchitecture.data.model.MyName
import es.rudo.rudokotlinarchitecture.modules.trivia.TriviaActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val MyName: MyName =
        MyName("Start")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myName = MyName


        binding.buttonStart.setOnClickListener{
            val intent = Intent(this, TriviaActivity::class.java)
            startActivity(intent)
        }
    }
}