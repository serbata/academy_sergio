package es.rudo.rudokotlinarchitecture.modules.social_share

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import es.rudo.rudokotlinarchitecture.R
import es.rudo.rudokotlinarchitecture.databinding.ActivitySocialShareBinding


class SocialShareActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySocialShareBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_social_share) //Binds the view

        var bitmap = createBitmapFromLayout(binding.constraintContent)
        Glide.with(applicationContext).load(bitmap).into(binding.imageResult)
    }

    private fun createBitmapFromLayout(view: View): Bitmap? {
        val spec: Int = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(spec, spec)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        val bitmap = Bitmap.createBitmap(
            view.measuredWidth, view.measuredWidth,
            Bitmap.Config.ARGB_8888
        )
        val c = Canvas(bitmap)
        c.translate((-view.scrollX).toFloat(), (-view.scrollY).toFloat())
        view.draw(c)
        return bitmap
    }
}