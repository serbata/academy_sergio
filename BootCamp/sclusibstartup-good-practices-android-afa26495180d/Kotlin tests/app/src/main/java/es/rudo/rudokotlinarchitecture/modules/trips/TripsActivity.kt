package es.rudo.rudokotlinarchitecture.modules.trips

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.rudo.rudokotlinarchitecture.MainActivity
import es.rudo.rudokotlinarchitecture.R
import es.rudo.rudokotlinarchitecture.adapters.TripsPaginationAdapter
import es.rudo.rudokotlinarchitecture.databinding.ActivityTripsBinding

class TripsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTripsBinding
    private lateinit var viewmodel: TripsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trips)
        viewmodel = ViewModelProvider(this).get(TripsViewModel::class.java)
        binding.tripsViewModel = viewmodel
        binding.tripsActivity = this

//        val adapter = TripsAdapter()  //Old adapter
//        val adapter = TripsAdapterDiffUtil(TripsAdapterDiffUtil.TripListener { _tripId ->
//            viewmodel.onTripClicked(_tripId)
//        })

        val adapter = TripsPaginationAdapter(object : TripsPaginationAdapter.TripListener {
            override fun onClick(cityId: Int) {
            }

            override fun onDeleted(cityId: Int) {
            }
        })

//        val adapter = TripsAdapterDiffUtil(object : TripsAdapterDiffUtil.TripListener {
//            override fun onClick(cityId: Int) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onDeleted(cityId: Int) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//        })

        binding.recyclerTrips.adapter = adapter
        adapter.addLoading()

        viewmodel.cities.observe(this, Observer {
            it?.let {
                //                adapter.data = it // set data on old adapter
                adapter.submitList(it)
            }
        })

        viewmodel.navigateToTripDetail.observe(this, Observer {
            it?.let {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        })
    }
}