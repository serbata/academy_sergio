package es.rudo.rudokotlinarchitecture.modules.login

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Room
import es.rudo.rudokotlinarchitecture.App
import es.rudo.rudokotlinarchitecture.data.model.Login
import es.rudo.rudokotlinarchitecture.data.model.User
import es.rudo.rudokotlinarchitecture.api.Config
import es.rudo.rudokotlinarchitecture.api.RetrofitClient
import es.rudo.rudokotlinarchitecture.data.local.AppDatabase
import es.rudo.rudokotlinarchitecture.data.local.DBLogin
import es.rudo.rudokotlinarchitecture.helpers.Constants
import es.rudo.rudokotlinarchitecture.helpers.Utils
import es.rudo.rudokotlinarchitecture.helpers.extensions.isValidEmail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    val username = MutableLiveData<String>().apply { postValue("") }
    val password = MutableLiveData<String>().apply { postValue("") }
    val usernameError = MutableLiveData<String>().apply { postValue("") }
    val passwordError = MutableLiveData<String>().apply { postValue("") }
    var myUser = MutableLiveData<User>()

    // Login button
    private var _eventLoginError = MutableLiveData<Boolean>()
    val eventLoginError: LiveData<Boolean>
        get() = _eventLoginError

    // Error on login
    private var _eventLoginCorrect = MutableLiveData<Boolean>()
    val eventLoginCorrect: LiveData<Boolean>
        get() = _eventLoginCorrect

    fun postLogin() {
        val login = Login()
        login.client_id = Config.CLIENT_ID
        login.client_secret = Config.CLIENT_SECRET
        login.grant_type = Config.GRANT_TYPE_LOGIN
        login.username = username.value
        login.password = password.value

        if (Utils.isNetworkAvailable(App.instance)) {
            CoroutineScope(Dispatchers.IO).launch {
                val responseLogin: Response<Login> = retrofitClient.postLogin(login)
                if (responseLogin.code() == Constants.SERVER_SUCCESS_CODE) {

                    /**
                     * Room working example
                     */
                    val db = Room.databaseBuilder(
                        App.instance,
                        AppDatabase::class.java, "app-database.db"
                    ).build()
                    var dbLogin = DBLogin(1, "", "", "", "", "", "", "")
                    dbLogin?.access_token = responseLogin.body()?.access_token.toString()
                    db.loginDao().insert(dbLogin)
                    db.loginDao().getLogin()

                    App.preferences.setAccessToken(responseLogin.body()?.access_token)
                    App.preferences.setRefreshToken(responseLogin.body()?.refresh_token)
                    getMe()
                }
            }
        } else {
            Toast.makeText(App.instance, "No network available", Toast.LENGTH_LONG).show()
        }
    }


    private fun getMe() {
        if (Utils.isNetworkAvailable(App.instance)) {
            CoroutineScope(Dispatchers.IO).launch {
                val responseMe = retrofitClient.getMe()
                if (responseMe.code() == Constants.SERVER_SUCCESS_CODE) {
                    withContext(Dispatchers.Main) {
                        //Cuando queremos setear un value hay que pasar al main thread, sino la app disparará un crash
//                        myUser.value = responseMe.body() //Set User object
                        App.preferences.setUserId(responseMe.body()?.id)
                        _eventLoginCorrect.value = true
                    }
                }
            }
        }
    }

    fun checkLogin() {
        _eventLoginError.value = false
        if (username.value?.isEmpty()!!) {
            _eventLoginError.value = true
            usernameError.value = "Este campo no puede ser vacío"
        } else if (!username.value!!.isValidEmail()) {
            _eventLoginError.value = true
            usernameError.value = "Formato de émail no válido"
        }
        if (password.value.toString().isEmpty()) {
            _eventLoginError.value = true
            passwordError.value = "Este campo no puede ser vacío"
        }
        if (!_eventLoginError.value!!) {
            postLogin()
        }
    }

//    private fun isEmailValid(): Boolean {
//        return username.value.toString().matches(Constants.EMAIL_PATTERN.toRegex())
//    }

}