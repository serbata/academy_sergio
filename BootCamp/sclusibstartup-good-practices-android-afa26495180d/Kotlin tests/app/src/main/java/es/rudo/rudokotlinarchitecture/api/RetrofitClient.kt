package es.rudo.rudokotlinarchitecture.api

import androidx.databinding.library.BuildConfig
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import es.rudo.rudokotlinarchitecture.data.model.Login
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitClient {

    private val clientWithAuth by lazy {
        Retrofit.Builder()
            .baseUrl(Config.API_URL)
            .client(
                OkHttpClient()
                    .newBuilder()
                    .addInterceptor(AccessTokenInterceptor())
                    .addInterceptor(
                        LoggingInterceptor.Builder()
                            .loggable(BuildConfig.DEBUG)
                            .setLevel(Level.BODY)
                            .request("Request")
                            .response("Response")
                            .addHeader("Accept", "application/json")
                            .build()
                    )
                    .build()
            )
            .addConverterFactory(MoshiConverterFactory.create()) //Moshi
            .build().create(Api::class.java)
    }

    private val clientWithoutAuth by lazy {
        Retrofit.Builder()
            .baseUrl(Config.API_URL)
            .client(
                OkHttpClient()
                    .newBuilder()
                    .addInterceptor(
                        LoggingInterceptor.Builder()
                            .loggable(BuildConfig.DEBUG)
                            .setLevel(Level.BODY)
                            .request("Request")
                            .response("Response")
                            .addHeader("Accept", "application/json")
                            .build()
                    )
                    .build()
            )
//            .addCallAdapterFactory(CoroutineCallAdapterFactory()) //Deprecated
            .addConverterFactory(MoshiConverterFactory.create()) //Moshi
//            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())) // Gson
            .build().create(Api::class.java)
    }



    suspend fun postLogin(login: Login) = clientWithoutAuth.postLogin(login)
    suspend fun getMe() = clientWithAuth.getMe()
    suspend fun getCities() = clientWithAuth.getCities()

}