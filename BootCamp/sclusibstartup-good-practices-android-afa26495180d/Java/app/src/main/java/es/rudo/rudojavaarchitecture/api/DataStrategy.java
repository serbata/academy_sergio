package es.rudo.rudojavaarchitecture.api;

import java.util.List;

import es.rudo.rudojavaarchitecture.entities.Login;
import es.rudo.rudojavaarchitecture.entities.Profile;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public abstract class DataStrategy {

    public abstract Call<Token> refreshToken();

    public abstract void getFirebaseToken(InteractDispatcherGeneric interactDispatcherGeneric);

    public abstract void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject);

    public abstract void postLogin(Login login, InteractDispatcherObject interactDispatcherObject);

    public abstract void getMe(InteractDispatcherObject interactDispatcherObject);

    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface InteractDispatcherListObject<T> {
        void response(int code, List<T> object);
    }

    public interface InteractDispatcherGeneric {
        void response(int code, String message);
    }

    public interface InteractDispatcherPager<T> {
        void response(int code, Pager<T> pager);
    }

    public interface ApiService {

        //REFRESH TOKEN
        @POST("auth/token/")
        Call<Token> refreshToken(@Body Login login);

        //GET FIREBASE TOKEN
        @GET("/users/firebase_token/")
        Call<String> getFirebaseToken();

        //REGISTER
        @POST("auth/register/")
        Call<Profile> postRegister(@Body Profile profile);

        //LOGIN
        @POST("auth/token/")
        Call<Token> postLogin(@Body Login login);

        //GET ME
        @GET("users/me/")
        Call<Profile> getMe();
    }
}