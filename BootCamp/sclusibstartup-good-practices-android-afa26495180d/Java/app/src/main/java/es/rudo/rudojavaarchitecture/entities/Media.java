package es.rudo.rudojavaarchitecture.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Media implements Serializable, Parcelable {

    private String file;
    private String thumbnail;
    private String midsize;
    private String fullsize;

    public Media(){

    }

    protected Media(Parcel in) {
        file = in.readString();
        thumbnail = in.readString();
        midsize = in.readString();
        fullsize = in.readString();
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMidsize() {
        return midsize;
    }

    public void setMidsize(String midsize) {
        this.midsize = midsize;
    }

    public String getFullsize() {
        return fullsize;
    }

    public void setFullsize(String fullsize) {
        this.fullsize = fullsize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(file);
        parcel.writeString(thumbnail);
        parcel.writeString(midsize);
        parcel.writeString(fullsize);
    }
}
