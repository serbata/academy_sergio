package es.rudo.rudojavaarchitecture.utils;

import android.content.Context;
import android.content.SharedPreferences;

import es.rudo.rudojavaarchitecture.App;

public class AppPreferences {

    private static final String PREF_FILE = "MyPreferences";

    private static final String ACCESS_TOKEN = "access_token";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String USER_ID = "user_id";
    private static final String USER_FIRST_NAME = "user_first_name";
    private static final String USER_LAST_NAME = "user_last_name";
    private static final String USER_PHOTO = "user_photo";
    private static final String USER_COVER = "user_cover";
    private static final String USER_EMAIL = "user_email";
    private static final String FIREBASE_ID = "firebase_id";

    private Context context;

    public AppPreferences() {

    }

    private SharedPreferences getSharedPreferences() {
        if (context != null) {
            return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        } else {
            return App.getInstance().getApplicationContext().getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        }
    }

    public String getUserId() {
        return getSharedPreferences().getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        this.getSharedPreferences().edit().putString(USER_ID, userId).apply();
    }

    public String getAccessToken() {
        return getSharedPreferences().getString(ACCESS_TOKEN, null);
    }

    public void setAccessToken(String accessToken) {
        this.getSharedPreferences().edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public String getRefreshToken() {
        return getSharedPreferences().getString(REFRESH_TOKEN, null);
    }

    public void setRefreshToken(String refreshToken) {
        this.getSharedPreferences().edit().putString(REFRESH_TOKEN, refreshToken).apply();
    }

    public String getUserFirstName() {
        return getSharedPreferences().getString(USER_FIRST_NAME, null);
    }

    public void setUserFirstName(String userFirstName) {
        this.getSharedPreferences().edit().putString(USER_FIRST_NAME, userFirstName).apply();
    }

    public String getUserLastName() {
        return getSharedPreferences().getString(USER_LAST_NAME, null);
    }

    public void setUserLastName(String userLastName) {
        this.getSharedPreferences().edit().putString(USER_LAST_NAME, userLastName).apply();
    }

    public String getUserPhoto() {
        return getSharedPreferences().getString(USER_PHOTO, null);
    }

    public void setUserPhoto(String userPhoto) {
        this.getSharedPreferences().edit().putString(USER_PHOTO, userPhoto).apply();
    }

    public String getUserCover() {
        return getSharedPreferences().getString(USER_COVER, null);
    }

    public void setUserCover(String userCover) {
        this.getSharedPreferences().edit().putString(USER_COVER, userCover).apply();
    }

    public String getUserEmail() {
        return getSharedPreferences().getString(USER_EMAIL, null);
    }

    public void setUserEmail(String userEmail) {
        this.getSharedPreferences().edit().putString(USER_EMAIL, userEmail).apply();
    }

    public String getFireBaseId() {
        return getSharedPreferences().getString(FIREBASE_ID, null);
    }

    public void setFirebaseId(String firebaseId) {
        this.getSharedPreferences().edit().putString(FIREBASE_ID, firebaseId).apply();
    }

    public void clearPreferences() {
        this.getSharedPreferences().edit().clear().apply();
    }

}
