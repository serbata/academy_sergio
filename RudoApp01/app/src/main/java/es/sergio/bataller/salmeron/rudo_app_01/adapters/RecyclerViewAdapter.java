package es.sergio.bataller.salmeron.rudo_app_01.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo.Profile;
import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.databinding.CardViajesItemBinding;
import es.sergio.bataller.salmeron.rudo_app_01.modelo.Viajes;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViajesVH> {
    private Context context;
    private List<Profile> objects;


    @NonNull
    @Override
    public ViajesVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardViajesItemBinding ViewViajesBinding = CardViajesItemBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new ViajesVH(ViewViajesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViajesVH holder, int position) {
  //      Profile profile = objects.get(position);
 //  holder.binding.cardImageBig.setImageResource(R.drawable.arboles);
 //  holder.binding.viajesTextCity.setText(profile.getCountry());
 //  holder.binding.viajesTextCountry.setText(profile.getCountry());
 //  holder.binding.textPrimerafecha.setText(String.valueOf(profile.getStartDate()));
 //  holder.binding.textSegundafecha.setText(String.valueOf(profile.getUserTrip().getEndDate()));
    }

    @Override
    public int getItemCount() {
            return objects.size();
    }

    public RecyclerViewAdapter(Context context, List<Profile> objects) {
        this.context = context;
        this.objects = objects;
    }

    public class ViajesVH extends RecyclerView.ViewHolder {
        private CardViajesItemBinding binding = null;
        public ViajesVH(CardViajesItemBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }
}
