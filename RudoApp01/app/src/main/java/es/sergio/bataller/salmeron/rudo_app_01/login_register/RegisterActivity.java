package es.sergio.bataller.salmeron.rudo_app_01.login_register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataWebService;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo.Profile;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.utils.Constraints;
import es.sergio.bataller.salmeron.rudo_app_01.MainActivity;
import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity {
    private ActivityRegisterBinding binding;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Drawable customErrorDrawable ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        customErrorDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.baseline_warning_black_18dp);
        initListener();
        isValidate();

    }

    private void initListener() {
        binding.buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (isValidate()) callRegiter();
            }
        });
    }

    private void callRegiter() {
        binding.buttonRegister.setEnabled(false);
        Profile register = new Profile();
        register.setEmail(binding.registerEmail.getText().toString());
        register.setUsername(binding.registerUsername.getText().toString());
        register.setPassword(binding.registerPassword.getText().toString());
        register.setPassword_confirm(binding.registerPassword2.getText().toString());


        new DataWebService().postRegister(register, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
        if (code == Constraints.SERVER_CREATED_CODE){
            Toast.makeText(RegisterActivity.this, "Usuario registrado con exito", Toast.LENGTH_SHORT).show();
            goToMainActivity();
        }else{
            Toast.makeText(RegisterActivity.this, "Invalid Register", Toast.LENGTH_SHORT).show();
            binding.buttonRegister.setEnabled(true);
        }
            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private boolean isValidate() {
        boolean isCorrect = true;
                if (binding.registerUsername.getText().toString().isEmpty()){
                    binding.registerUsername.setError("Falta rellenar datos");
                }
                else if (binding.registerUsername.getText().toString().trim().matches(emailPattern)){
                    binding.registerUsername.setError("Esto no es el email. Cambialo Porfavor");
                    binding.registerUsername.requestFocus();
                    isCorrect=false;
                }
                if (binding.registerEmail.getText().toString().isEmpty()) {
                    binding.registerEmail.setError("Falta rellenar el Email");
                    binding.registerEmail.requestFocus();
                    isCorrect=false;
                } else if (binding.registerEmail.getText().toString().trim().matches(emailPattern)) {
                    binding.registerEmail.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.sharp_done_outline_black_18dp,0);
                    binding.registerEmail.getResources().getColor(R.color.WellColor, null);
                } else {
                    binding.registerEmail.setError("Introduce un email valido");
                    binding.registerEmail.requestFocus();
                    isCorrect=false;
                }
                if (!binding.registerPassword.getText().toString().isEmpty() &&
                        !binding.registerPassword.getText().toString().isEmpty()) {
                    if (binding.registerPassword.getText().toString().equals(binding.registerPassword2.getText().toString())) {
                        Toast.makeText(RegisterActivity.this, "Muy Bien Sabes Poner 2 contraseñas IGUALES!", Toast.LENGTH_SHORT).show();
                    } else {
                        binding.registerPassword.setError("No coinciden las contraseñas");
                        binding.registerPassword2.setError("No coinciden las contraseñas");
                        binding.registerPassword.requestFocus();
                        binding.registerPassword2.requestFocus();
                        isCorrect=false;
                    }
                    if (binding.registerPassword.getText().toString().trim().matches(emailPattern)){
                        binding.registerPassword.setError("No es un email");
                        isCorrect=false;
                    }else if (binding.registerPassword2.getText().toString().trim().matches(emailPattern)){
                        binding.registerPassword2.setError("No es un email");
                        isCorrect=false;
                    }
                }
                if (binding.checkboxTerminos.isChecked()) {
                    Toast.makeText(RegisterActivity.this, "Gracias por confiar en nosotros", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RegisterActivity.this, "Tienes que confirmar los terminos y condiciones de la página", Toast.LENGTH_SHORT).show();
                    isCorrect=false;
                }
        return isCorrect;
    }
}