package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;

public class Media implements Serializable {
    private Type type;
    private int id;
    private Medias imageMedias;
    private Object video;
    private Medias videoThumbnailMedias;
    private OffsetDateTime createdAt;
    private String user;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Medias getImageMedias() {
        return imageMedias;
    }

    public void setImageMedias(Medias imageMedias) {
        this.imageMedias = imageMedias;
    }

    public Object getVideo() {
        return video;
    }

    public void setVideo(Object video) {
        this.video = video;
    }

    public Medias getVideoThumbnailMedias() {
        return videoThumbnailMedias;
    }

    public void setVideoThumbnailMedias(Medias videoThumbnailMedias) {
        this.videoThumbnailMedias = videoThumbnailMedias;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
