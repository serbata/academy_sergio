package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;
import java.util.List;

public class Cities implements Serializable {
    private long count;
    private Object next;
    private Object previous;
    private List<Destination> results;


    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<Destination> getResults() {
        return results;
    }

    public void setResults(List<Destination> results) {
        this.results = results;
    }
}
