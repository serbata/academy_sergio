package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;

public class SocialRegister implements Serializable {
    private String username;
    private String firstName;
    private String lastName;
    private String id;
    private String email;
    private Object deviceID;
    private String platform;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Object deviceID) {
        this.deviceID = deviceID;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
