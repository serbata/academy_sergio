package es.sergio.bataller.salmeron.rudo_app_01.ejemplos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.coordinatorlayout.widget.ViewGroupUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import de.hdodenhof.circleimageview.CircleImageView;
import es.sergio.bataller.salmeron.rudo_app_01.R;

public class Ejemplo02Activity extends AppCompatActivity {
    private CircleImageView imageView_profile,imageView_country;
    private AppBarLayout appbar;
    private FloatingActionButton profile_fab_settings,profile_fab_settings02;
    private String txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setting);
        appbar = findViewById(R.id.ejemplo_appbar);
        profile_fab_settings = findViewById(R.id.profile_fab_settings);
        profile_fab_settings02=findViewById(R.id.profile_fab_settings02);
        imageView_profile=findViewById(R.id.profle_profile_image);
        imageView_country=findViewById(R.id.profile_country_image);
   //     imageView_profile = findViewById(R.id.profle_profile_image);
   //     CoordinatorLayout.LayoutParams params = imageView_profile.getLayoutParams();
   //     params.setBehavior(new CircleImageViewBehavior());
        txtNombre = "Juan P";

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    //Collapsed
                    profile_fab_settings.setVisibility(View.VISIBLE);
                    profile_fab_settings02.setVisibility(View.GONE);

                } else if (verticalOffset == 0) {
                    //Expanded
                    profile_fab_settings.setVisibility(View.GONE);
                    profile_fab_settings02.setVisibility(View.VISIBLE);
                } else {
                    profile_fab_settings.setEnabled(true);
                    profile_fab_settings02.setEnabled(false);
                }
            }
        });
    }
}



