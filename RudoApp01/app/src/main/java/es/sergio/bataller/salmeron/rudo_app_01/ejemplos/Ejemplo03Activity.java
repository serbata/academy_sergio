package es.sergio.bataller.salmeron.rudo_app_01.ejemplos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.Random;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataWebService;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.Token;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo.Profile;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.utils.Constraints;
import es.sergio.bataller.salmeron.rudo_app_01.App;
import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.adapters.ViewPageAdapter;
import es.sergio.bataller.salmeron.rudo_app_01.fragment.HomeFragment;
import es.sergio.bataller.salmeron.rudo_app_01.fragment.MapsFragment;

public class Ejemplo03Activity extends AppCompatActivity {
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView txtTab1, txtTab2;
    private TabLayout tabLayout;
    ArrayList<Fragment> fragments;
    ViewPager viewPager;
    final int min = 20;
    final int max = 80;
    final int random = new Random().nextInt((max - min) + 1) + min;
    final int random2 = new Random().nextInt((max - min) + 1) + min;
    final int random3 = new Random().nextInt((max - min) + 1) + min;
    private ViewPager2 viewPager2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo03);
        tabLayout = findViewById(R.id.ejemplo3_tablayout);
        fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new MapsFragment());
        fragments.add(new HomeFragment());
        viewPager2= findViewById(R.id.ejemplo3_pager);
        viewPager2.setAdapter(new ViewPageAdapter(this));

       initRecogeDatos();
        TabViewPager();

    }

 private void initRecogeDatos() {
     TextView txtBio =findViewById(R.id.ejemplo3_bio);

     new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
         @Override
         public void response(int code, Object object) {
             if (code == Constraints.SERVER_SUCCESS_CODE){
                 Profile profile = (Profile) object;

             }
         }
     });
 }

    private void TabViewPager() {
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(
                tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position){
                    case 0: {
                        tab.setCustomView(R.layout.custom_tab_example);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setText("POSTS");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText(String.valueOf(random));
                        break;
                    }
                    case 1: {

                        tab.setCustomView(R.layout.custom_tab_example);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setText("VIAJES");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText(String.valueOf(random2));
                        break;
                    }
                    case 2: {

                        tab.setCustomView(R.layout.custom_tab_example);
                        txtTab1 = (tab.getCustomView().findViewById(R.id.tab_item_text1));
                        txtTab1.setText("AMIGOS");
                        txtTab2 = (tab.getCustomView().findViewById(R.id.tab_item_text2));
                        txtTab2.setText(String.valueOf(random3));
                        BadgeDrawable badgeDrawable = tab.getOrCreateBadge();
                        badgeDrawable.setBackgroundColor(
                                ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)
                        );
                        badgeDrawable.setVisible(true);
                        badgeDrawable.setNumber(100);
                        badgeDrawable.setMaxCharacterCount(3);
                        break;
                    }
                }
            }
        }
        );
        tabLayoutMediator.attach();

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                BadgeDrawable badgeDrawable = tabLayout.getTabAt(position).getOrCreateBadge();
                badgeDrawable.setVisible(false);
            }
        });
    }


}