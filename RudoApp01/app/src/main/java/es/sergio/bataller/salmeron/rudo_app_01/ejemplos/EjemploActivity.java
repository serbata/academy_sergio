package es.sergio.bataller.salmeron.rudo_app_01.ejemplos;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.modelo.User;

public class EjemploActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private AppBarLayout appbar;
    private FloatingActionButton btnFab, fab_settings, fab_collapse_settings;
    private CollapsingToolbarLayout ctlLayout;
    private Toolbar toolbar;
    private User user;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo);
        appbar = findViewById(R.id.app_bar);
        fab_settings = findViewById(R.id.fab_settings);
        fab_collapse_settings = findViewById(R.id.fab_collapse_settings);
        //TabLayout
        tabLayout = (TabLayout) findViewById(R.id.appbartabs);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab().setText("02 " + "\n" + "POSTS"));


        tabLayout.addTab(tabLayout.newTab().setText("03" + "\n" + "VIAJES"));
        tabLayout.addTab(tabLayout.newTab().setText("0" + "\n" + "AMIGOS"));

        toolbar = findViewById(R.id.detail_toolbar);
        //Floatin Action Button

        btnFab = findViewById(R.id.fab_photos);
        btnFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "This is a PRANK", Snackbar.LENGTH_LONG).show();
            }
        });
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("Juan Garcia");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.colorHint));
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(0, 0, 0));

        // ctlLayout= findViewById(R.id.ctlLayout);
        //ctlLayout.setTitle("This is not A PRANK");

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    //Collapsed
                    fab_settings.setEnabled(false);
                    fab_collapse_settings.setEnabled(true);

                } else if (verticalOffset == 0) {
                    //Expanded
                    fab_settings.setEnabled(true);
                    fab_collapse_settings.setEnabled(false);
                } else {
                    fab_settings.setEnabled(true);
                    fab_collapse_settings.setEnabled(false);
                }
            }
        });
    }
}