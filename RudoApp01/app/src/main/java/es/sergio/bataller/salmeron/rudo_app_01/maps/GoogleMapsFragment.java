package es.sergio.bataller.salmeron.rudo_app_01.maps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import es.sergio.bataller.salmeron.rudo_app_01.R;

public class GoogleMapsFragment extends Fragment {

    private SupportMapFragment mapFragment;
    protected GoogleMap map;
    private Marker previousMarker = null;
    ImageView image_spain, image_irland,image_lastone;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

       View v= inflater.inflate(R.layout.fragment_google_maps, container, false);
       mapFragment= (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
       if(mapFragment == null){
           FragmentManager fm =getFragmentManager();
           FragmentTransaction ft =fm.beginTransaction();
           mapFragment=SupportMapFragment.newInstance();
           ft.replace(R.id.map,mapFragment).commit();
       }
       mapFragment.getMapAsync(new OnMapReadyCallback() {
           @Override
           public void onMapReady(GoogleMap googleMap) {
               map = googleMap;
               LatLng sydney = new LatLng(-34, 151);
               LatLng valencia = new LatLng(39.4702,-0.376805);
               map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
               map.getUiSettings().setZoomControlsEnabled(true);
               map.getUiSettings().setZoomGesturesEnabled(true);
               map.getUiSettings().setCompassEnabled(true);
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                   if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                   == PackageManager.PERMISSION_GRANTED)
                   {
                       initListener();
                        map.addMarker(new MarkerOptions()
                        .position(valencia)
                        .title("Valencia")
                        .rotation((float) -15.0)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        );
                        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                String locAddress = marker.getTitle();
                                if (previousMarker != null ){
                                    previousMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                }
                                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                                previousMarker = marker;
                                return true;
                            }
                        });
                        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng point) {
                                MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title("New Marker");
                                map.addMarker(marker);
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, (float)15.0));
                                System.out.println(point.latitude+"---"+ point.longitude);
                            }
                        });
                       map.setMyLocationEnabled(true);
                   }
               }
               else{

                   map.setMyLocationEnabled(true);
               }
           }
       });



        return v;
    }

    private void initListener() {
        LatLng spain=new LatLng(40.463667,-3.74922);
        LatLng irland = new LatLng(53.41291,-8.24389);
        LatLng lastone = new LatLng(53.826,-2.422);
        image_irland.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkerOptions markerIrland = new MarkerOptions().position(irland).title("Irland");
                map.addMarker(markerIrland);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(irland,(float)15.0));
            }
        });
        image_spain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkerOptions markerSpain = new MarkerOptions().position(spain).title("Spain");
                map.addMarker(markerSpain);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(spain,(float)6.0));
            }
        });
        image_lastone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkerOptions markerLast = new MarkerOptions().position(lastone).title("StoneHead");
                map.addMarker(markerLast);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(lastone,(float)15.0));
            }
        });
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

    }
}