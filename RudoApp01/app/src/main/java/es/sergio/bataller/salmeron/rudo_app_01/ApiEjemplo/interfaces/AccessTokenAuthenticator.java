package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import es.sergio.bataller.salmeron.rudo_app_01.App;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;

public class AccessTokenAuthenticator implements Authenticator {
    @Nullable
    @Override
    public Request authenticate(@Nullable Route route, @NotNull Response response) throws IOException {
        final String accesToken = App.preferences.getAccessToken();
        if (!isRequestWithAccessToken(response) || accesToken == null) {
            return null;
        }
        synchronized (this) {
            final String newAccesToken = App.preferences.getAccessToken();
            //Access token is resfresh in another thread
            if (!accesToken.equals(newAccesToken)) {
                return newRequestWithAccessToken(response.request(), newAccesToken);
            }
            //Need to refresh an acces token
            Call<Token> call = new DataWebService().refreshToken();
            retrofit2.Response<Token> res = call.execute();
            Token data = res.body();
            if (data != null) {
                final String updatedAccesToken = data.getAccess_token();
                final String updateRefreshToken = data.getRefresh_token();
                App.preferences.setAccessToken(updatedAccesToken);
                App.preferences.setRefreshToken(updateRefreshToken);
                return newRequestWithAccessToken(response.request(), updatedAccesToken);
            } else {
                return newRequestWithAccessToken(response.request(), App.preferences.getAccessToken());
            }
        }
    }

    private boolean isRequestWithAccessToken(@NonNull Response response) {
        String header = response.request().header("Authorization");
        return header != null && header.startsWith("Bearer");
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .build();
    }
}
