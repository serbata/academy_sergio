package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.utils;

public class RequestCode {
    // Put here your request codes
    public static final int NEW_TRIP_REQUEST_CODE = 100;
    public static final int FIND_CITY_REQUEST_CODE = 101;
    public static final int PHOTOSET_REQUEST_CODE = 102;
    public static final int UPLOAD_MEDIA_REQUEST_CODE = 103;
    public static final int EDIT_PROFILE = 104;
    public static final int ALBUM_DELETED = 105;
}
