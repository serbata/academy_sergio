package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;

public class Blocked_Users implements Serializable {
    private int id_user_blocked;
    private String blocked_users;

    public int getId_user_blocked() {
        return id_user_blocked;
    }

    public void setId_user_blocked(int id_user_blocked) {
        this.id_user_blocked = id_user_blocked;
    }

    public String getBlocked_users() {
        return blocked_users;
    }

    public void setBlocked_users(String blocked_users) {
        this.blocked_users = blocked_users;
    }
}
