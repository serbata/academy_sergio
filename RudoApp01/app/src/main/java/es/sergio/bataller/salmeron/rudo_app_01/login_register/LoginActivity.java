package es.sergio.bataller.salmeron.rudo_app_01.login_register;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.Configuracion;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataStrategy;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.DataWebService;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.interfaces.Token;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo.Login;
import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.utils.Constraints;
import es.sergio.bataller.salmeron.rudo_app_01.App;
import es.sergio.bataller.salmeron.rudo_app_01.MainActivity;
import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.databinding.ActivityLoginBinding;
import es.sergio.bataller.salmeron.rudo_app_01.ejemplos.Ejemplo03Activity;

public class LoginActivity extends AppCompatActivity{
    private Context context;
    private Drawable customErrorDrawable ;
    private App app;
    private ActivityLoginBinding binding;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        customErrorDrawable=  ContextCompat.getDrawable(getApplicationContext(), R.drawable.twotone_report_gmailerrorred_white_18);
        customErrorDrawable.setBounds(0, 0, customErrorDrawable.getIntrinsicWidth(), customErrorDrawable.getIntrinsicHeight());
        initLogin();
        initListener();

    }

    private void initListener() {
        binding.textForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Le acabamos de enviar un email de recuperación", Toast.LENGTH_SHORT).show();
            }
        });
        binding.textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
        binding.buttonCordinator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, Ejemplo03Activity.class);
                startActivity(intent);
            }
        });
    }

    private void initLogin() {
        binding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.editUsername.getText().toString().isEmpty() &&
                        binding.editPassword.getText().toString().isEmpty()) {
                    binding.editPassword.setError("Rellena los campos", customErrorDrawable);
                    binding.editUsername.setError("Rellena los campos", customErrorDrawable);
                }
                else{
                   binding.buttonLogin.setEnabled(false);
                   Login login = new Login();
                   login.setUsername(binding.editUsername.getText().toString());
                   login.setPassword(binding.editPassword.getText().toString());
                   login.setGrant_type(Configuracion.GRANT_TYPE_LOGIN);
                   new DataWebService().postLogin(login, new DataStrategy.InteractDispatcherObject(){

                       @Override
                       public void response(int code, Object object) {


                        if (code == Constraints.SERVER_SUCCESS_CODE) {

                           Token token=(Token) object;


                            String texto = token.getAccess_token();
                            App.preferences.setAccessToken(token.getRefresh_token());

                            Toast.makeText(LoginActivity.this, texto, Toast.LENGTH_SHORT).show();
                            goToEjemplo03();

                       // goToMainActivity();
                        }else {
                            Toast.makeText(LoginActivity.this, "Fallo al logearse", Toast.LENGTH_SHORT).show();
                            binding.buttonLogin.setEnabled(true);
                        }
                       }
                   });
                }
            }
        });
    }

    private void goToEjemplo03() {
        Intent intent = new Intent(LoginActivity.this,Ejemplo03Activity.class);
        startActivity(intent);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }


}