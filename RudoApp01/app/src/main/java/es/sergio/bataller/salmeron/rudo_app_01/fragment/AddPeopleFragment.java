package es.sergio.bataller.salmeron.rudo_app_01.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.databinding.FragmentAddPeopleBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddPeopleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPeopleFragment extends Fragment implements View.OnClickListener {
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String phonePattern = "^\\+[0-9]{9,13}$ ";
    FragmentAddPeopleBinding binding;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddPeopleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddPeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddPeopleFragment newInstance(String param1, String param2) {
        AddPeopleFragment fragment = new AddPeopleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        initGuardarUser();

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_people, container, false);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initGuardarUser();
    }

    private void initGuardarUser() {
        binding.buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.addUserNombre.getText().toString().isEmpty()){
                    binding.addUserNombre.setError("Debes rellenar este campo");
                }else if(binding.addUserPhone2.getText().toString().isEmpty()) {
                    binding.addUserPhone2.setError("Debes rellenar este campo");
                }else if (binding.addUserUser2.getText().toString().isEmpty()){
                    binding.addUserUser2.setError("Debes rellenar este campo");
                }else if(binding.addUserEmailv2.getText().toString().isEmpty()){
                    binding.addUserEmailv2.setError("Debes rellenar este campo");
                }else if(binding.addUserEmailv2.getText().toString().trim().matches(emailPattern)){
                    Toast.makeText(getContext(), "El email esta correcto", Toast.LENGTH_SHORT).show();
                }

/**
                if (binding.addUserEmailv2.getText().toString().isEmpty()) {
                    binding.addUserEmailv2.setError("Debes rellenar este campo");
                } else if (binding.addUserEmailv2.getText().toString().trim().matches(emailPattern)) {
                    binding.addUserEmailv2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sharp_done_outline_black_18dp, 0);
                    binding.addUserEmailv2.getResources().getColor(R.color.WellColor, null);
                }
                if (binding.addUserNombre.getText().toString().isEmpty()) {
                    binding.addUserNombre.setError("Debes rellenar este campo");
                } else if (binding.addUserNombre.getText().toString().trim().matches(emailPattern)) {
                    binding.addUserNombre.setError("Este campo no es un email");
                }
                if (binding.addUserPhone2.getText().toString().isEmpty()) {
                    binding.addUserPhone2.setError("Debes rellenar este campo");
                } else if (binding.addUserPhone2.getText().toString().trim().matches(emailPattern)) {
                    binding.addUserPhone2.setError("Esto no es un email");
                } else if (binding.addUserPhone2.getText().toString().trim().matches(phonePattern)) {
                    binding.addUserPhone2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sharp_done_outline_black_18dp, 0);
                    binding.addUserPhone2.getResources().getColor(R.color.WellColor, null);
                }
                if (binding.addUserUser2.getText().toString().isEmpty()) {
                    binding.addUserUser2.setError("Debes rellenar este campo");
                }
**/
            }

    });
}


    @Override
    public void onClick(View v) {
        initGuardarUser();
    }
}