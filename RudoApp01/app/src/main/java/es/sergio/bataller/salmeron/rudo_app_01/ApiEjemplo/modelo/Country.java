package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;

public class Country implements Serializable {
    private String name;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
