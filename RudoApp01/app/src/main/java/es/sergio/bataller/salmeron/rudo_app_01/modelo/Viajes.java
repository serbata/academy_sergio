package es.sergio.bataller.salmeron.rudo_app_01.modelo;

import java.io.Serializable;

public class Viajes implements Serializable {
    private int viaje_id;
    private String viaje_name;
    private String viaje_country;
    private String viaje_firstDate;
    private String viaje_secondDate;

    public Viajes() {
    }

    public Viajes(int viaje_id,  String viaje_name, String viaje_country, String viaje_firstDate, String viaje_secondDate) {
        this.viaje_id = viaje_id;
        this.viaje_name = viaje_name;
        this.viaje_country = viaje_country;
        this.viaje_firstDate = viaje_firstDate;
        this.viaje_secondDate = viaje_secondDate;
    }

    public int getViaje_id() {
        return viaje_id;
    }

    public void setViaje_id(int viaje_id) {
        this.viaje_id = viaje_id;
    }


    public String getViaje_name() {
        return viaje_name;
    }

    public void setViaje_name(String viaje_name) {
        this.viaje_name = viaje_name;
    }

    public String getViaje_country() {
        return viaje_country;
    }

    public void setViaje_country(String viaje_country) {
        this.viaje_country = viaje_country;
    }

    public String getViaje_firstDate() {
        return viaje_firstDate;
    }

    public void setViaje_firstDate(String viaje_firstDate) {
        this.viaje_firstDate = viaje_firstDate;
    }

    public String getViaje_secondDate() {
        return viaje_secondDate;
    }

    public void setViaje_secondDate(String viaje_secondDate) {
        this.viaje_secondDate = viaje_secondDate;
    }
}
