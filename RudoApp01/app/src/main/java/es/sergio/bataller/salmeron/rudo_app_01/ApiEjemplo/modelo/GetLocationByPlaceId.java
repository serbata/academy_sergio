package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;

public class GetLocationByPlaceId implements Serializable {
    private long id;
    private String name;
    private Country country;
    private Medias medias;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Medias getMedias() {
        return medias;
    }

    public void setMedias(Medias medias) {
        this.medias = medias;
    }
}
