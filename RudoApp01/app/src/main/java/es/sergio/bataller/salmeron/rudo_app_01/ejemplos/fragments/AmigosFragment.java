package es.sergio.bataller.salmeron.rudo_app_01.ejemplos.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.adapters.FragmentAdapter;
import es.sergio.bataller.salmeron.rudo_app_01.modelo.User;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AmigosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AmigosFragment extends Fragment {
    private List<User> listaUsers;
    private RecyclerView contenedor;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentAdapter adapter;

    public AmigosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AmigosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AmigosFragment newInstance(String param1, String param2) {
        AmigosFragment fragment = new AmigosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listaUsers = new ArrayList<>();
        listaUsers.add(new User(1,"serbata","rake@hotmail.com","1234","Pepe","65462737"));
        listaUsers.add(new User(2,"serbata2","rake1@hotmail.com","1233","Juan","65462737"));
        listaUsers.add(new User(3,"serbata3","rake2@hotmail.com","1232","Silvia","65462737"));
        listaUsers.add(new User(4,"serbata4","rake3@hotmail.com","1231","Maria","65462737"));
        listaUsers.add(new User(5,"serbata5","rake4@hotmail.com","1235","TIA","65462737"));
        listaUsers.add(new User(6,"serbata6","rake5@hotmail.com","1236","PEP","65462737"));
        listaUsers.add(new User(7,"serbata7","rake6@hotmail.com","1237","Manolito","65462737"));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_amigos, container, false);
        contenedor = v.findViewById(R.id.recycler_amigos);
        adapter = new FragmentAdapter(getContext(), listaUsers);
        contenedor.setLayoutManager(new LinearLayoutManager(getActivity()));
        contenedor.setAdapter(adapter);
        // Inflate the layout for this fragment
        return v;
    }
}