package es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.modelo;

import java.io.Serializable;
import java.util.List;

public class Trips implements Serializable {
    private long count;
    private Object next;
    private Object previous;
    private List<TripsResultado> results;



    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<TripsResultado> getResults() {
        return results;
    }

    public void setResults(List<TripsResultado> results) {
        this.results = results;
    }
}
