package es.sergio.bataller.salmeron.rudo_app_01;

import android.app.Application;

import es.sergio.bataller.salmeron.rudo_app_01.ApiEjemplo.utils.AppPreference;

public class App extends Application {
    private static final String FATAL_NO_INSTANCE = "Fatal Error: No App instance found";
    private static App instance;
    public static AppPreference preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = new AppPreference();
        instance = this;

    }

    public static synchronized App getInstance() {
        if (instance == null) {
            throw new IllegalArgumentException(FATAL_NO_INSTANCE);
        }
        return instance;
    }
}
