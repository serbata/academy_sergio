package es.sergio.bataller.salmeron.rudo_app_01;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


import com.google.android.material.bottomnavigation.BottomNavigationView;

import es.sergio.bataller.salmeron.rudo_app_01.ejemplos.EjemploActivity;
import es.sergio.bataller.salmeron.rudo_app_01.fragment.AddPeopleFragment;
import es.sergio.bataller.salmeron.rudo_app_01.fragment.HomeFragment;
import es.sergio.bataller.salmeron.rudo_app_01.fragment.PeopleFragment;
import es.sergio.bataller.salmeron.rudo_app_01.maps.GoogleMapsFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemReselectedListener {
    private Fragment fragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //    toolbar = findViewById(R.id.toolbar);
    //    toolbar.setTitle("Viaja entre Países");


        initMenu();
    }

    private void initMenu() {
        loadFragment(new PeopleFragment());
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemReselectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_location:
                fragment = new HomeFragment();
                break;
            case R.id.menu_comment:
                fragment = new AddPeopleFragment();
                break;
            case R.id.menu_world:
                fragment = new GoogleMapsFragment();
                break;
            case R.id.menu_profile:
                Intent intent = new Intent(MainActivity.this, EjemploActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_like:
                break;

        }
        loadFragment(fragment);
    }



    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


}