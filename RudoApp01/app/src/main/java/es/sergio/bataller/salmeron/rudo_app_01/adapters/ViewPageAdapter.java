package es.sergio.bataller.salmeron.rudo_app_01.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.HashMap;
import java.util.Map;

import es.sergio.bataller.salmeron.rudo_app_01.R;
import es.sergio.bataller.salmeron.rudo_app_01.ejemplos.fragments.AmigosFragment;
import es.sergio.bataller.salmeron.rudo_app_01.ejemplos.fragments.PostsFragment;
import es.sergio.bataller.salmeron.rudo_app_01.ejemplos.fragments.ViajesFragment;

public class ViewPageAdapter extends FragmentStateAdapter {


    public ViewPageAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new PostsFragment();
            case 1:
                return new ViajesFragment();
            default:
                return new AmigosFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    enum Tab {
        POSTS(0, R.string.tab_posts),
        VIAJES(1,R.string.tab_viajes),
        AMIGOS(2,R.string.tab_amigos);
        final int  title;
        final int position;

        Tab(int position, @StringRes int title) {
            this.position = position;
            this.title = title;
        }
    }
}
